---
layout: default
title: Analytics
nav_order: 3
description: "analytics tracking"
permalink: /ui-android/analytics
parent: UI Android
---
include::../../docs/config.adoc[]

=== Analytics tracking

Crumbs UI components allow you to listen to the basic events that occur in them and retrieve their attached value.

=== Listen events

To listen to UI events you need to register an *EventListener* in *CrumbsUI*.

[source,java,indent=0]
----
include::{android_demo_src}/DemoApp.kt[tags="eventListener"]
----

=== Event ids

Available event ids can be found in the Event class.

[source,java,indent=0]
----
include::{ui_android_src}/analytics/Event.kt[tags="eventIds"]
----