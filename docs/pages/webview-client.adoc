---
layout: default
title: Client
nav_order: 2
description: "WebView client"
permalink: /webview/client
parent: WebView
---
include::../../docs/config.adoc[]


=== Client setup

To enable Crumbs in your WebView you must set up a *PatWebViewClient*.

[source,java,indent=0]
----
include::{android_demo_src}/MainActivity.kt[tags="client"]
----

=== Listen stats

You can listen active WebView statistics with a listener on the client.
[source,java,indent=0]
----
include::{android_demo_src}/MainActivity.kt[tags="listen"]
----

=== Nested client

If you need to use a custom *WebViewClient* for your app you can pass it as a nested client to *CrumbsWebViewClient*.
[source,java,indent=0]
----
include::{android_demo_src}/MainActivity.kt[tags="nestedClient"]
----

include::../../docs/pages/ready.adoc[]