---
layout: default
title: Chromium
nav_order: 4
description: "WebView module"
permalink: /chromium/home
---
= Chromium
include::../../docs/config.adoc[]

== Repository

We provides a Chromium project with the PAT and Crumbs SDK already set up for you. You can use this project as a base to build your own browser.

link:https://gitlab.com/eyeo/pat/pat-chromium[PAT Chromium repository,role="btn btn-primary fs-5 mb-4 mb-md-0 mr-2"]

== Git patches

You can also download git patches from the repository https://gitlab.com/eyeo/pat/pat-chromium/-/packages[package registry].
These patches do not contain ABP filter engine code, so be sure to apply them on top of the ABP implementation.


== Setup


=== Sync
Once PAT code is in place you need to run *gclient sync* in order to retrieve PAT dependencies.


[source,bash]
----
gclient sync --with_branch_heads --reset --delete_unversioned_trees
----

This will download PAT libraries in *third_party/pat/libs* folder.


NOTE: Another valid option is to execute the synchronization by calling the following script : *python3 src/third_party/pat/fetch_all.py*

=== Version

PAT, Crumbs and Email mask versions can be changed in *third_party/pat/build.gradle*. Make sure the versions you upgrade are compatible with your current PAT Chromium implementation.

==== Latest versions

patVersion = image:https://img.shields.io/badge/dynamic/xml?label=version&query=%2F%2Fversioning%2Frelease&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fgroups%2F10262605%2F-%2Fpackages%2Fmaven%2Fcom%2Feyeo%2Fpat%2fpat-core%2Fmaven-metadata.xml[Version,caption="PAT Version",link=https://gitlab.com/eyeo/pat/pat-sdk/-/packages]

crumbsVersion = image:https://img.shields.io/badge/dynamic/xml?label=version&query=%2F%2Fversioning%2Frelease&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fgroups%2F8621009%2F-%2Fpackages%2Fmaven%2Forg%2Fcrumbs%2Fcrumbs-ui-android%2Fmaven-metadata.xml[Version,caption="Crumbs Version",link=https://gitlab.com/eyeo/crumbs/crumbs-sdk/-/packages]

emailMaskVersion = image:https://img.shields.io/badge/dynamic/xml?label=version&query=%2F%2Fversioning%2Frelease&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fgroups%2F64738657%2F-%2Fpackages%2Fmaven%2Fcom%2Feyeo%2Femailmask%2femail-mask-core%2Fmaven-metadata.xml[Version,caption="Email Mask Version",link=https://gitlab.com/eyeo/email-mask/email-mask-sdk/-/packages]


=== Build parameters

PAT plugins are off by default. To enable them, add the following parameters to your *args.gn* file.


[source,properties]
----
enable_crumbs = true
enable_email_mask = true
----


include::../../docs/pages/ready.adoc[]
