---
layout: default
title: Get started
nav_order: 1
description: "How to integrate crumbs core in an application"
permalink: /setup
---
include::../../docs/config.adoc[]

== Integration libraries

Crumbs provides multiple SDKs to facilitate integration.

=== Android integration libraries

Be it *WebView* or *Chromium* based apps, accelerate your development by using our pre established UI components and integration library detailed below.

=== link:chromium/home[Chromium Integration]
=== link:webview/home[Android WebView Integration]
=== link:ui-android/home[Android UI]

'''

== Manual integration

If your application is not supported by one of our integration libraries and you want to integrate Crumbs SDK in an *Android* or *Javascript* project, here are the required steps you need to follow:

=== Setup Crumbs

==== *On Android*

- *Setup the dependencies*

crumbsCoreVersion = image:https://img.shields.io/badge/dynamic/xml?label=version&query=%2F%2Fversioning%2Frelease&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fgroups%2F8621009%2F-%2Fpackages%2Fmaven%2Forg%2Fcrumbs%2Fcrumbs-core-android%2Fmaven-metadata.xml[Version,caption="Crumbs Core Version",link=https://gitlab.com/eyeo/crumbs/crumbs-core/-/packages/]

[source,groovy]
----
repository {
    maven { url "https://gitlab.com/api/v4/groups/8621009/-/packages/maven" }
}
dependences {
    implementation "org.crumbs:crumbs-core-android:$crumbsCoreVersion"
}
----

- *Initialize CrumbsCore at the start of the application*

If CrumbsCore is not initialised at the start of the application, the SDK will crash when the instance is requested.

[source,kotlin,indent=0]
----
include::{core_instrumented_test_path}/CrumbsAndroidTest.kt[tags="setup"]
----

- *Check if Crumbs has been initialized*

[source,kotlin,indent=0]
----
include::{core_instrumented_test_path}/CrumbsAndroidTest.kt[tags="isInitialized"]
----

- *Retrieve the instance*

[source,kotlin,indent=0]
----
include::{core_instrumented_test_path}/CrumbsAndroidTest.kt[tags="instance"]
----

'''

==== *On Javascript*

- *Setup dependencies*

[source,bash]
----
npm install crumbs-core --save
----

- *Retrieve the instance*

[source,js]
----
import crumbsCore from 'crumbs-core-kotlin';
----

'''

=== Plug in Crumbs

Crumbs makes use of the *PAT SDK* library to plug-in its functionality to Android WebView and Chromium projects.

link:https://gitlab.com/eyeo/pat/pat-sdk[PAT SDK on GitLab,role="btn btn-primary fs-5 mb-4 mb-md-0 mr-2"]
