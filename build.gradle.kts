@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.download) apply false
    alias(libs.plugins.dokka) apply false
    alias(libs.plugins.npm.publish) apply false
    alias(libs.plugins.pat.plugin)
}

pat {
    ci {
        setup = true
    }
}

val crumbsVersion = libs.versions.crumbs.library.full.get()

allprojects {
    group = "org.crumbs"
    version = crumbsVersion
}