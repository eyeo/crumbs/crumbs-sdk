package org.crumbs.utils

import android.os.Build
import java.util.*

fun Locale.toBCP47LanguageTag(): String {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        return this.toLanguageTag()
    }

    val separator = '-'
    var lang = this.language
    var region = this.country
    var variant = this.variant

    // filter out special case for Norwegian Nynorsk since "NY" cannot be a variant as per BCP 47
    if (lang == "no" && region == "NO" && variant == "NY") {
        lang = "nn"
        region = "NO"
        variant = ""
    }
    if (lang.isEmpty() || !lang.matches("\\p{Alpha}{2,8}".toRegex())) {
        // "und" stands for Undetermined as described in Locale#toLanguageTag
        lang = "und"
    } else if (lang == "iw") { // fix deprecated language codes
        lang = "he"
    } else if (lang == "in") {
        lang = "id"
    } else if (lang == "ji") {
        lang = "yi"
    }

    // validate country code
    if (!region.matches("\\p{Alpha}{2}|\\p{Digit}{3}".toRegex())) {
        region = ""
    }

    // validate variant subtags
    if (!variant.matches("\\p{Alnum}{5,8}|\\p{Digit}\\p{Alnum}{3}".toRegex())) {
        variant = ""
    }
    val tag = StringBuilder(lang)
    if (region.isNotEmpty()) {
        tag.append(separator).append(region)
    }
    if (variant.isNotEmpty()) {
        tag.append(separator).append(variant)
    }
    return tag.toString()
}