package org.crumbs.utils

import java.util.*

actual fun codeToCountryName(code: String): String = Locale("", code).displayCountry