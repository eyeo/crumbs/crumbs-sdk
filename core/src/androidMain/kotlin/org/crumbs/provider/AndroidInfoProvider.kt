package org.crumbs.provider

import android.content.Context
import android.os.Build
import com.eyeo.pat.PatLogger
import com.eyeo.pat.utils.ContextExtension.getVersionCode
import com.eyeo.pat.utils.ContextExtension.getVersionName
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.crumbs.models.AppInfo
import org.crumbs.models.ProfileDeviceInfo
import org.crumbs.utils.toBCP47LanguageTag
import java.util.*

internal class AndroidInfoProvider(private val context: Context) : PlatformInfoProvider {

    override suspend fun isLimitAdTrackingEnabled(): Boolean {
        return withContext(Dispatchers.IO) {
            return@withContext try {
                val advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(context)
                advertisingIdInfo.isLimitAdTrackingEnabled
            } catch (e: Throwable) {
                logger.w("Can't retrieve limitAdTracking device settings", e)
                false
            }
        }
    }

    override fun getPlatformLanguages(): String {
        val allLanguageTags = arrayListOf<String>()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val locales = context.resources.configuration.locales
            for (i in 0 until locales.size()) {
                allLanguageTags.add(locales[i].toBCP47LanguageTag())
            }
        } else {
            allLanguageTags.add(Locale.getDefault().toBCP47LanguageTag())
        }
        return allLanguageTags.takeIf { it.isNotEmpty() }?.joinToString { ", " } ?: ""
    }

    override fun getDeviceInfo(): ProfileDeviceInfo =
        ProfileDeviceInfo("mobile", "Android ${Build.VERSION.RELEASE}")

    override fun getAppInfo(): AppInfo =  AppInfo(
        name = context.packageName,
        version = "${context.getVersionName()} (${context.getVersionCode()})}"
    )

    companion object {
        private val logger = PatLogger("AndroidInfoProvider")
    }
}