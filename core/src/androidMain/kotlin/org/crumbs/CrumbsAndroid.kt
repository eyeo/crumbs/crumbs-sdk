package org.crumbs

import android.content.Context
import com.eyeo.pat.provider.AndroidResourceProvider
import com.eyeo.privacy.PrivacyShieldAndroid
import com.russhwolf.settings.SharedPreferencesSettings
import org.crumbs.migration.PreferencesMigration
import org.crumbs.provider.AndroidInfoProvider

@Suppress("unused")
class CrumbsAndroid {

    companion object {

        const val SETTINGS_NAME = "crumbs"

        @JvmStatic
        fun get() = CrumbsCore.get()

        @JvmStatic
        @JvmOverloads
        fun setup(
            context: Context,
            crumbsOptions: CrumbsOptions = CrumbsOptions()
        ) {
            val privacyShieldSettings = SharedPreferencesSettings.Factory(context)
                .create(PrivacyShieldAndroid.SETTINGS_NAME)
            val crumbsSettings =
                SharedPreferencesSettings.Factory(context).create(SETTINGS_NAME)
            PreferencesMigration.migratePreferences(crumbsSettings, privacyShieldSettings)

            PrivacyShieldAndroid.setup(context, crumbsOptions.privacyShieldOptions)

            CrumbsCore.createInstance(
                PrivacyShieldAndroid.get(),
                crumbsSettings,
                AndroidInfoProvider(context),
                AndroidResourceProvider(context),
                crumbsOptions,
            )
        }

        @JvmStatic
        fun release() {
            CrumbsCore.destroy()
            PrivacyShieldAndroid.release()
        }

        @JvmStatic
        fun isInitialized() = CrumbsCore.isInitialized()
    }
}