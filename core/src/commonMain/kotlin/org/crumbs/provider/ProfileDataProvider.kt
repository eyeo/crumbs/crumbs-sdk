package org.crumbs.provider

import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.StoredValue
import org.crumbs.models.ProfileData
import org.crumbs.models.ProfileDeviceInfo
import org.crumbs.models.ProfileLocation
import org.crumbs.utils.codeToCountryName

internal class ProfileDataProvider(
    storageProvider: StorageProvider,
    private val platformInfoProvider: PlatformInfoProvider,
) {

    private val profile = StoredValue(
        key = PROFILE_KEY,
        storageProvider = storageProvider,
        serializer = ProfileData.serializer(),
        defaultBuilder = { ProfileData() }
    )

    fun listen() = profile.listen()

    fun get() = profile.value

    suspend fun updateProfile() {
        val lmt = platformInfoProvider.isLimitAdTrackingEnabled()
        val acceptLanguage = platformInfoProvider.getPlatformLanguages()
        val device = platformInfoProvider.getDeviceInfo()
        profile.store(profile.value.apply {
            this.lmt = lmt
            this.acceptLanguage = acceptLanguage
            this.device = device
        })
    }

    fun setDeviceInfo(deviceInfo: ProfileDeviceInfo) {
        profile.store(profile.value.apply {
            this.device = deviceInfo
        })
    }

    fun setLanguages(acceptLanguage: String?) {
        acceptLanguage?.takeIf { it != profile.value.acceptLanguage }?.let {
            profile.store(profile.value.apply {
                this.acceptLanguage = acceptLanguage
            })
        }
    }

    fun setLocation(countryCode: String?) {
        countryCode?.takeIf { it != profile.value.location?.countryCode }?.let {
            profile.store(profile.value.apply {
                this.location = ProfileLocation(codeToCountryName(countryCode), countryCode)
            })
        }
    }

    fun setUserAgent(userAgent: String?) {
        userAgent?.takeIf { it != profile.value.userAgent }?.let {
            profile.store(profile.value.apply {
                this.userAgent = userAgent
            })
        }
    }

    companion object {
        private const val PROFILE_KEY = "crumbs_profile_data"
    }
}