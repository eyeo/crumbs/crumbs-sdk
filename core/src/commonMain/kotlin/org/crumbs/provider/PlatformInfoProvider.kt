package org.crumbs.provider

import org.crumbs.models.AppInfo
import org.crumbs.models.ProfileDeviceInfo

internal interface PlatformInfoProvider {

    suspend fun isLimitAdTrackingEnabled(): Boolean

    fun getPlatformLanguages(): String

    fun getDeviceInfo(): ProfileDeviceInfo

    fun getAppInfo(): AppInfo

}