package org.crumbs

import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.Coroutines
import com.eyeo.pat.utils.SystemTimeProvider
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.presenter.PrivacyPresenter
import com.eyeo.privacy.presenter.StatsPresenter
import com.russhwolf.settings.Settings
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.crumbs.models.InterestsSettings
import org.crumbs.presenter.*
import org.crumbs.provider.*
import org.crumbs.service.*
import kotlin.coroutines.CoroutineContext
import kotlin.js.JsExport

@JsExport
class CrumbsCore internal constructor(
    private val privacyShieldCore: PrivacyShieldCore,
    private val platformInfoProvider: PlatformInfoProvider,
    crumbsSettingsStorage: Settings,
    resourcesProvider: ResourcesProvider,
    httpClientProvider: HttpClientProvider = HttpClientProvider(),
    crumbsOptions: CrumbsOptions
) {

    private val privacy: PrivacyPresenter = privacyShieldCore.privacy()
    private val stats: StatsPresenter = privacyShieldCore.stats()
    private val analytics =  privacyShieldCore.analytics()

    private val interests: InterestsPresenter

    private val crumbsPlugin: CrumbsPlugin
    private val crumbsStorageProvider: StorageProvider
    private val coroutineContext: CoroutineContext = Coroutines.getSingleThreadContext()
    private val jobs: Job

    init {
        val appInfo = platformInfoProvider.getAppInfo()
        crumbsStorageProvider = StorageProvider(crumbsSettingsStorage)

        val interestsSettings = SettingsProvider(
            INTERESTS_SETTINGS_KEY,
            crumbsStorageProvider,
            InterestsSettings.serializer(),
            defaultBuilder = { InterestsSettings() },
            version = 2
        )
        val timeProvider = SystemTimeProvider()
        val metaDataProvider =
            MetaDataProvider(
                coroutineContext,
                httpClientProvider,
                crumbsStorageProvider,
                timeProvider
            )
        val profileDataProvider = ProfileDataProvider(crumbsStorageProvider, platformInfoProvider)
        val interestsService =
            InterestsService(
                interestsSettings,
                metaDataProvider,
                timeProvider,
                coroutineContext,
                crumbsStorageProvider
            )
        val safeToShareService =
            SafeToShareService(
                crumbsStorageProvider,
                timeProvider,
                httpClientProvider,
                profileDataProvider,
                crumbsOptions.safeToShareListUrl,
                appInfo,
            )
        val profileService =
            ProfileService(
                interestsService,
                safeToShareService,
                { privacy.getMainDomain(it) },
                profileDataProvider,
                metaDataProvider
            )
        val partnerService = PartnersService(
            timeProvider,
            httpClientProvider,
            crumbsStorageProvider,
            crumbsOptions.partnersListUrl
        )
        val privacySettings = privacy.getSettingsProvider()
        val reportService = ReportService(
            coroutineContext,
            profileService, privacySettings = privacySettings,
            httpClientProvider, resourcesProvider,
            appInfo,
            crumbsStorageProvider,
            timeProvider,
        )

        interests = InterestsPresenter(interestsService, profileService)

        crumbsPlugin = CrumbsPlugin(
            privacySettings,
            interestsService,
            partnerService,
            profileService,
        )

        jobs = CoroutineScope(coroutineContext).launch {
            crumbsPlugin.setup()

            profileDataProvider.updateProfile()

            delay(1000L)

            var metaDataJob: Job? = null
            var partnersJob: Job? = null
            var reportJob: Job? = null
            var interestsJob: Job? = null
            var safeToShareJob: Job? = null
            interestsService.listenSettings()
                .map { it.enabled }
                .distinctUntilChanged()
                .onEach { enabled ->
                    metaDataJob?.cancel()
                    partnersJob?.cancel()
                    reportJob?.cancel()
                    interestsJob?.cancel()
                    safeToShareJob?.cancel()
                    if (enabled) {
                        metaDataJob = launch { while (isActive) delay(metaDataProvider.runCron()) }
                        partnersJob = launch { while (isActive) delay(partnerService.runCron()) }
                        reportJob = launch { while (isActive) delay(reportService.runCron()) }
                        interestsJob = launch { while (isActive) delay(interestsService.runCron()) }
                        safeToShareJob =
                            launch { while (isActive) delay(safeToShareService.runCron()) }
                    }
                }.launchIn(this)
        }
    }

    fun privacy() = privacy

    fun interests() = interests

    fun stats() = stats

    @Suppress("NON_EXPORTABLE_TYPE")
    fun analytics() = analytics

    val version = CoreBuildConfig.version

    private fun destroy() {
        jobs.cancel()
        crumbsPlugin.destroy()
    }

    companion object {

        private var instance: CrumbsCore? = null

        /**
         * visible for test only
         */
        internal fun setTestInstance(testInstance: CrumbsCore?) {
            instance = testInstance
        }

        @Suppress("NON_EXPORTABLE_TYPE")
        fun storageProvider() = get().crumbsStorageProvider

        internal fun get(): CrumbsCore {
            instance?.let {
                return it
            }
            throw IllegalStateException("Crumbs not initialized ! Setup crumbs before any usage")
        }

        internal fun isInitialized(): Boolean = instance != null

        internal fun createInstance(
            privacyShieldCore: PrivacyShieldCore,
            crumbsSettings: Settings,
            platformInfoProvider: PlatformInfoProvider,
            resourcesProvider: ResourcesProvider,
            crumbsOptions: CrumbsOptions,
        ) {
            if (isInitialized()) throw IllegalStateException("Crumbs is already initialized")
            instance = CrumbsCore(
                privacyShieldCore = privacyShieldCore,
                platformInfoProvider = platformInfoProvider,
                crumbsSettingsStorage = crumbsSettings,
                resourcesProvider = resourcesProvider,
                crumbsOptions = crumbsOptions
            )
        }

        internal fun destroy() {
            instance?.destroy()
            instance = null
        }

        internal const val CRUMBS_WEBSITE = "https://crumbs.org"
        private const val INTERESTS_SETTINGS_KEY = "crumbs_insights_settings"
    }

}