package org.crumbs.presenter

import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.pat.models.SettingsChangeListener
import com.eyeo.pat.provider.SettingsEditor
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.utils.Coroutines.toCallback
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import com.eyeo.pat.utils.UrlExtension.toUrl
import io.ktor.http.*
import kotlinx.coroutines.flow.*
import org.crumbs.models.*
import org.crumbs.service.InterestsService
import org.crumbs.service.ProfileService
import kotlin.js.JsExport
import kotlin.js.JsName
import kotlin.jvm.JvmOverloads

/**
 * This presenter is the entry point to use the interests feature
 */
@Suppress("unused")
@JsExport
class InterestsPresenter internal constructor(
    private val interestsService: InterestsService,
    private val profileService: ProfileService,
) {

    /**
     * listen shared interests on a specific web page
     *
     * @param tabId id of the tab
     * @param documentUrl url of the web content
     * @param callback return the shared interests in the page if any
     */
    fun listenSharedInterests(
        tabId: String,
        documentUrl: String,
        callback: (Array<CrumbsInterest>) -> Unit
    ): Cancellable {
        val url = documentUrl.toUrl()
        return profileService.listenSharedInterests(tabId, url ?: Url(""))
            .toCallback {
                callback(it.toTypedArray())
            }
    }

    /**
     * listen user profile interests data change.
     *
     * @param fetchAll if false (default) interests with not enough data will be ignored
     * @param callback call each time the user profile is edited and return the new current profile
     */
    fun listenProfileInterests(
        fetchAll: Boolean = false,
        callback: (Array<ProfileInterest>) -> Unit
    ): Cancellable {
        return profileService.listenCrumbsProfile()
            .combine(interestsService.listenSettings().map { it.enabled }
                .distinctUntilChanged(),
                transform = { profile, enable ->
                    profile.takeIf { enable }?.let {
                        val toProfileInterests = it.toProfileInterests(fetchAll)
                        toProfileInterests
                    } ?: emptyList()
                })
            .toCallback {
                callback(it.toTypedArray())
            }
    }

    /**
     * Set the enable state of an interest.
     * Once disabled, an interest won't appear again in the user profile
     *
     * @param interestId id of the interest to edit
     * @param enable true to enable the interest, false otherwise
     */
    fun setInterestState(interestId: Int, enable: Boolean) {
        interestsService.setInterestState(interestId, enable)
    }

    /**
     * Reset the collected data about this interest in the user profile.
     *
     * @param interestId id of the interest to reset
     */
    fun resetInterest(interestId: Int) {
        interestsService.resetInterest(interestId)
    }

    /**
     * Get the interests feature settings
     *
     * @return interests settings
     */
    fun getSettings() = interestsService.getSettings().copy()

    /**
     * Edit the interests feature settings
     *
     * @return settings editor
     */
    fun editSettings() = Editor(interestsService.getSettingsProvider())

    /**
     * Listen the interests settings changes
     *
     * @return a settings flow
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    fun listenSettings() = interestsService.listenSettings()

    /**
     * @see listenSettings
     *
     * @param callback will be called on the main thread each time the interests settings update
     *
     * @return a cancellable object to end listening
     */
    @JsName("listenSettingsWithCallback")
    fun listenSettings(callback: (InterestsSettings) -> Unit) =
        listenSettings().toCallback(callback)

    /**
     * @see listenSettings
     *
     * @param changeListener will be called on the main thread every time the interests settings update
     *
     * @return a cancellable object to end listening
     */
    @JsName("listenSettingsWithListener")
    fun listenSettings(changeListener: SettingsChangeListener<InterestsSettings>) =
        listenSettings { settings -> changeListener.onSettingsChanged(settings) }

    /**
     * Sets a fake shareable profile that will be forcefully shared with partners according to
     * the partners list, or null to disable this functionality.
     *
     * @see [org.crumbs.CrumbsOptions]
     *
     * @param shareableProfile the fake profile
     *
     */
    @JvmOverloads
    fun setFakeShareableProfile(
        shareableProfile: ShareableProfile? = ShareableProfile(
            country = "IS",
            interests = listOf(
                60,  // Design
                251, // Computer & Video Games
                572  // Extreme Sports
            )
        )
    ) {
        profileService.setFakeShareableProfile(shareableProfile)
    }

    class Editor internal constructor(settingsProvider: SettingsProvider<InterestsSettings>) :
        SettingsEditor<InterestsSettings>(settingsProvider) {

        fun enable(enabled: Boolean) = this.apply {
            value = value.copy(enabled = enabled)
        }

        fun enableDomain(url: String) = apply {
            disabledDomains(value.disabledDomains.toHashSet().apply {
                url.getUrlHost()?.let {
                    remove(it)
                }
            }.toTypedArray())
            disabledDomainsThisSession(value.disabledDomainsThisSession.toHashSet().apply {
                url.getUrlHost()?.let {
                    remove(it)
                }
            }.toTypedArray())
        }

        fun disableDomain(url: String) = apply {
            disabledDomains(
                value.disabledDomains.toHashSet()
                    .apply {
                        url.getUrlHost()?.let {
                            add(it)
                        }
                    }.toTypedArray()
            )
        }

        fun disableDomainThisSession(url: String) = apply {
            disabledDomainsThisSession(
                value.disabledDomainsThisSession.toHashSet()
                    .apply {
                        url.getUrlHost()?.let {
                            add(it)
                        }
                    }.toTypedArray()
            )
        }

        fun resetDisabledDomainsThisSession() = apply {
            value = value.copy(disabledDomainsThisSession = setOf())
        }

        fun disabledDomains(domains: Array<String>) = apply {
            value = value.copy(disabledDomains = domains.toSet())
        }

        fun disabledDomainsThisSession(domains: Array<String>) = apply {
            value = value.copy(disabledDomainsThisSession = domains.toSet())
        }
    }
}
