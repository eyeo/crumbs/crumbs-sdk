package org.crumbs

@Suppress("unused")
interface CrumbsProvider {

    val crumbs: CrumbsCore?
        get() = testInstance ?: if (CrumbsCore.isInitialized()) CrumbsCore.get() else null
    val requireCrumbs: CrumbsCore
        get() = testInstance ?: CrumbsCore.get()

    val privacy
        get() = crumbs?.privacy()
    val requirePrivacy
        get() = requireCrumbs.privacy()

    val interests
        get() = crumbs?.interests()
    val requireInterests
        get() = requireCrumbs.interests()

    val stats
        get() = crumbs?.stats()
    val requireStats
        get() = requireCrumbs.stats()

    companion object {
        /**
         * Visible for test only
         */
        var testInstance: CrumbsCore? = null
    }

}