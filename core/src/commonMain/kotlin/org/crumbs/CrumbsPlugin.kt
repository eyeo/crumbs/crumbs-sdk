package org.crumbs

import com.eyeo.pat.browser.BrowserPluginApi
import com.eyeo.pat.browser.plugins.*
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.PrivacyShieldHeaders
import com.eyeo.privacy.service.Header
import io.ktor.http.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.serialization.json.Json
import org.crumbs.models.CrumbsHeaders
import org.crumbs.models.ShareableProfile
import org.crumbs.service.*

@Suppress("unused")
class CrumbsPlugin internal constructor(
    private val privacySettings: SettingsProvider<PrivacySettings>,
    private val interestsService: InterestsService,
    private val partnersService: PartnersService,
    private val profileService: ProfileService,
) : HeadersPlugin, HistoryListener {

    fun setup() {
        BrowserPluginApi.Headers.registerPlugin(this)

        if (!interestsService.processedHistory()) {
            BrowserPluginApi.History.queryHistory(InterestsService.DAY_OF_HISTORY, this)
        }
    }

    fun destroy() {
        BrowserPluginApi.Headers.unregisterPlugin(this)
    }

    override fun rewriteRequestHeaders(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        requestType: RequestType,
        incognito: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {

        headers.filter { it.first == PrivacyShieldHeaders.UserAgent }.forEach {
            profileService.profile.setUserAgent(it.second)
        }
        headers.filter { it.first == PrivacyShieldHeaders.AcceptLanguage }.forEach {
            profileService.profile.setLanguages(it.second)
        }

        return headers.addInterestHeaderIfRequired(tabId, documentUrl, requestUrl, incognito)
    }

    override fun rewriteResponseHeaders(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        requestType: RequestType,
        incognito: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {
        return headers.addInterestHeaderIfRequired(tabId, documentUrl, requestUrl, incognito)
    }

    /**
     *  will be called when browser history is ready to build the first profile
     *
     * @param visits the history visits
     */
    override fun onHistoryReady(visits: Array<Visit>) {
        interestsService.onHistoryReady(visits.toList())
    }

    private fun getInterestsHeader(
        tabId: String,
        docUrl: Url,
        doNotTrack: Boolean
    ): Header? {
        val profile =
            profileService.getShareableProfile(tabId, docUrl, doNotTrack)
        return profile?.let { CrumbsInterestsHeader(profile) }
    }


    class CrumbsInterestsHeader(private val value: ShareableProfile) :
        Header(CrumbsHeaders.XCrumbsInterests) {

        companion object {
            private val json = Json {
                ignoreUnknownKeys = true
                prettyPrint = false
            }

            fun profileToJson(value: ShareableProfile): String {
                return json.encodeToString(ShareableProfile.serializer(), value)
            }
        }

        override fun getValue(): String = profileToJson(value)
    }

    private fun shouldShareInterests(documentUrl: Url, requestUrl: Url) =
        interestsService.getSettings()
            .isSharingEnabledWith(documentUrl.host) && partnersService.shouldShareInterests(
            requestUrl,
            PartnersService.UrlType.ResponseUrl
        )

    private fun List<Pair<String, String>>.addInterestHeaderIfRequired(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        incognito: Boolean
    ): List<Pair<String, String>> {
        if (!incognito && shouldShareInterests(documentUrl, requestUrl)) {
            val interestHeader = getInterestsHeader(
                tabId,
                documentUrl,
                privacySettings.get().enableDoNotTrack
            )
            interestHeader?.let {
                return this +
                        Pair(it.getName(), it.getValue()) +
                        Pair(CrumbsHeaders.CacheControl, "no-store")
            }
        }
        return this
    }
}