package org.crumbs.service

import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import org.crumbs.models.CrumbsInterest
import org.crumbs.models.ProfileData
import org.crumbs.models.ProfileInterest
import org.crumbs.models.ShareableProfile
import org.crumbs.provider.MetaDataProvider
import org.crumbs.provider.ProfileDataProvider

internal class ProfileService internal constructor(
    private val interestsService: InterestsService,
    private val safeToShareService: SafeToShareService,
    private val getMainDomain: (Url) -> String,
    val profile: ProfileDataProvider,
    private val metaDataProvider: MetaDataProvider,
) {
    private val cachedShareableData = hashMapOf<String, ShareableProfile>()
    private val tabsUrlChannels = hashMapOf<String, MutableStateFlow<Url>>()
    private var fakeShareableProfile: ShareableProfile? = null

    /**
     * If Interests sharing is enabled, returns the shareable profile or null otherwise
     * @param tabId         the tab id
     * @param documentUrl   the document URL
     * @param doNotTrack    whether the Do Not Track request header is enforced
     */
    fun getShareableProfile(
        tabId: String,
        documentUrl: Url,
        doNotTrack: Boolean
    ): ShareableProfile? {
        if (!interestsService.getSettings().isSharingEnabledWith(documentUrl.host)) return null
        val mainDomain = getMainDomain(documentUrl)
        var data = cachedShareableData[mainDomain]
        // refresh cache if empty
        if (data == null) {
            fakeShareableProfile?.let {
                cachedShareableData[mainDomain] = it
            } ?: getCrumbsProfile().toShareableProfile(doNotTrack)
                ?.also { cachedShareableData[mainDomain] = it }

            data = cachedShareableData[mainDomain]
            getTabUrlsChannel(tabId, documentUrl).value = documentUrl
        }
        return data
    }

    fun listenSharedInterests(
        tabId: String,
        documentUrl: Url
    ): Flow<List<CrumbsInterest>> {
        val mainDomain = getMainDomain(documentUrl)
        return getTabUrlsChannel(tabId, documentUrl)
            .map { if (it == documentUrl) cachedShareableData[mainDomain] else null }
            .map { shareableProfile ->
                val interests = mutableListOf<CrumbsInterest>()
                shareableProfile?.interests?.let { shareableInterests ->
                    val metadataInterests = getCrumbsProfile().allowedMetadataInterests()
                        .filter { it.value?.isBlank() == false }
                    interests.addAll(metadataInterests)
                    interests.addAll(shareableInterests.mapNotNull { toCrumbsInterest(it) })
                }
                interests
            }.combine(
                interestsService.listenSettings()
                    .map { it.isSharingEnabledWith(documentUrl.host) }
                    .distinctUntilChanged(),
                transform = { sharedInterests, sharingEnabled ->
                    if (sharingEnabled) sharedInterests else emptyList()
                })
    }

    fun getRelevantInterestIds(): Set<Int> {
        return getCrumbsProfile().relevantInterests().keys
    }

    internal fun listenCrumbsProfile(): Flow<CrumbsProfile> {
        return profile.listen().combine(
            interestsService.listenSettings(),
            transform = { _, _ ->
                getCrumbsProfile()
            })
    }

    internal fun getCrumbsProfile(): CrumbsProfile {
        val settings = interestsService.getSettings()
        return CrumbsProfile(profile.get(), settings.interests, settings.blockedInterests)
    }

    private fun getTabUrlsChannel(tabId: String, initialValue: Url): MutableStateFlow<Url> {
        return tabsUrlChannels.getOrPut(tabId, defaultValue = {
            MutableStateFlow(initialValue)
        })
    }

    internal suspend fun toCrumbsInterest(catId: Int): CrumbsInterest? {
        return metaDataProvider.getInterest(catId)
    }

    fun setFakeShareableProfile(shareableProfile: ShareableProfile?) {
        fakeShareableProfile = shareableProfile
    }

    inner class CrumbsProfile internal constructor(
        val data: ProfileData,
        val interests: Map<Int, Float>,
        val blockedInterests: Set<Int>
    ) {
        /**
         * Returns the list of relevant profile interests and blocked interests sorted by the highest score
         */
        suspend fun toProfileInterests(fetchAll: Boolean): List<ProfileInterest> {
            return withContext(Dispatchers.Default) {

                val profileInterests = hashMapOf<Int, ProfileInterest>()

                // add blocked interests that exist
                profileInterests.putAll(
                    blockedInterests.mapNotNull { metaDataProvider.getInterest(it) }
                        .map {
                            it.id to ProfileInterest(
                                it,
                                enabled = false,
                                shareable = false,
                                score = 0f
                            )
                        }
                )

                val idsToScores = relevantInterests() + allowedMetadataInterestsRaw()
                toInterests()
                    .forEach {
                        val score = idsToScores[it.id]
                        profileInterests[it.id] =
                            ProfileInterest(
                                it,
                                enabled = true,
                                shareable = score != null,
                                score = score ?: 0f
                            )
                    }

                if (fetchAll) {
                    metaDataProvider.getInterests().forEach {
                        profileInterests.getOrPut(it.id, defaultValue = {
                            ProfileInterest(
                                it,
                                enabled = true,
                                shareable = false,
                                score = interests[it.id] ?: 0f
                            )
                        })
                    }
                }

                profileInterests.map { it.value }.toList().sortedBy { it.score }.reversed()
            }
        }

        private suspend fun toInterests(): List<CrumbsInterest> {
            return arrayListOf<CrumbsInterest>().apply {
                addAll(data.toInterests().filterNot { blockedInterests.contains(it.id) })
                val categories = relevantInterests().keys.asFlow()
                    .map { metaDataProvider.getInterest(it) }
                    .filterNotNull()
                    .flowOn(Dispatchers.Default)
                    .toList()
                addAll(categories)
            }
        }

        internal fun toShareableProfile(dnt: Boolean): ShareableProfile? {
            val safeToShareList = safeToShareService.getSafeToShareListData().safeToShareList
            val interestsIntersection =
                relevantInterests().keys.intersect(safeToShareList.interests.toSet())
            val clientMetaData =
                MetaData(data.userAgent, data.location?.countryCode, dnt, data.acceptLanguage)

            val areInterestsSafeToShare =
                interestsIntersection.size >= MIN_SHARABLE_INTERESTS_POOL_SIZE
            val isMetaDataSafeToShare = safeToShareList.metadata.contains(clientMetaData)

            return if (areInterestsSafeToShare && isMetaDataSafeToShare) {
                ShareableProfile(
                    clientMetaData.country,
                    interestsIntersection.shuffled().take(SHAREABLE_COUNT).toList()
                )
            } else {
                null
            }
        }

        private fun allowedInterests(): Map<Int, Float> {
            return interests.filterKeys { !blockedInterests.contains(it) }
        }

        fun relevantInterests(): Map<Int, Float> {
            return allowedInterests()
                .filterValues { it > INTEREST_RELEVANT_SCORE }
        }

        fun allowedMetadataInterests(): List<CrumbsInterest> {
            return data.toInterests().filterNot { blockedInterests.contains(it.id) }
        }

        private fun allowedMetadataInterestsRaw(): Map<Int, Float> {
            return allowedMetadataInterests().associate { it.id to Float.MAX_VALUE }
        }
    }

    companion object {
        internal const val INTEREST_RELEVANT_SCORE = 5
        internal const val MIN_SHARABLE_INTERESTS_POOL_SIZE = 3
        internal const val SHAREABLE_COUNT = 3
    }
}

