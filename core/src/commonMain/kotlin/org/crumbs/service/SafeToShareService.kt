package org.crumbs.service

import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.UpdatedStoredValue
import com.eyeo.pat.utils.SystemTimeProvider.Companion.lastModifiedToFormattedTimestamp
import com.eyeo.pat.utils.TimeProvider
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import org.crumbs.CoreBuildConfig
import org.crumbs.models.AppInfo
import org.crumbs.models.CrumbsHeaders
import org.crumbs.provider.ProfileDataProvider
import kotlin.math.max

@Serializable
internal data class MetaData(
    val ua: String?,
    val country: String?,
    val dnt: Boolean,
    val lang: String?
)

@Serializable
internal data class SafeToShareList(
    val interests: List<Int>,
    val metadata: List<MetaData>
)

@Serializable
internal data class SafeToShareListData(
    val safeToShareList: SafeToShareList,
    val lastVersion: String,
    val downloadCount: Long
)

/**
 * Responsible with the retrieval and storage of the safe to share list
 * containing safe to share metadata (like specific user agent, country and languages)
 * and specific interests.
 */
class SafeToShareService internal constructor(
    storageProvider: StorageProvider,
    timeProvider: TimeProvider,
    private val httpClientProvider: HttpClientProvider,
    private val profileDataProvider: ProfileDataProvider,
    private val safeToShareListUrl: String,
    private val appInfo: AppInfo
) {
    private var data = UpdatedStoredValue(
        key = SAFE_TO_SHARE_LIST_KEY,
        storageProvider = storageProvider,
        timeProvider = timeProvider,
        serializer = SafeToShareListData.serializer(),
        defaultBuilder = {
            SafeToShareListData(
                SafeToShareList(emptyList(), emptyList()),
                lastVersion = "",
                downloadCount = 0
            )
        }
    )

    internal fun getSafeToShareListData() = data.value

    suspend fun runCron(): Long {
        val forceUpdate =
            data.value.safeToShareList.interests.isEmpty() || data.value.safeToShareList.metadata.isEmpty()
        return data.update(forceUpdate) {
            val response: HttpResponse =
                httpClientProvider.client.get(safeToShareListUrl) {
                    parameter("addonName", ADDON_NAME)
                    parameter(
                        "addonVersion",
                        CoreBuildConfig.version
                    )
                    parameter("application", appInfo.name)
                    parameter("applicationVersion", appInfo.version)
                    parameter("lastVersion", data.value.lastVersion)
                    parameter("downloadCount", max(data.value.downloadCount, DOWNLOAD_COUNT_CAP))
                }
            if (response.status.isSuccess()) {
                val downloadCount = data.value.downloadCount + 1
                val safeToShareList = response.body<SafeToShareList>()
                val lastVersion = response.headers[HttpHeaders.LastModified]?.let {
                    lastModifiedToFormattedTimestamp(it)
                }.orEmpty()
                val countryCode = response.headers[CrumbsHeaders.XCrumbsCountryCode]
                withContext(Dispatchers.Default) {
                    data.store(SafeToShareListData(safeToShareList, lastVersion, downloadCount))
                    profileDataProvider.setLocation(countryCode)
                }
            }
        }
    }

    companion object {
        internal const val SAFE_TO_SHARE_LIST_KEY = "safe_to_share_list_key"
        internal const val DOWNLOAD_COUNT_CAP = 5L
        internal const val ADDON_NAME = "crumbs_mobile"
    }
}