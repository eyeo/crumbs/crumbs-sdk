package org.crumbs.service

import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.UpdatedStoredValue
import com.eyeo.pat.utils.TimeProvider
import com.eyeo.pat.utils.UrlExtension.toUrl
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlin.time.Duration.Companion.days

class PartnersService internal constructor(
    private val timeProvider: TimeProvider,
    private val clientProvider: HttpClientProvider,
    storageProvider: StorageProvider,
    private val partnersUrl: String
) {
    private val serviceParams = UpdatedStoredValue(
        key = PARTNER_SERVICE_PARAMS_KEY,
        storageProvider = storageProvider,
        timeProvider = timeProvider,
        serializer = ServiceParams.serializer(),
        defaultBuilder = { ServiceParams() },
        updateDelayMs = 7.days.inWholeMilliseconds,
    )

    internal suspend fun runCron(): Long {
        return serviceParams.update(serviceParams.value.partners.isEmpty()) {
            val partners = retrievePartners(partnersUrl)
            serviceParams.store(
                serviceParams.value.copy(
                    lastUpdateTime = timeProvider.getCurrentTimeMs(),
                    partners = partners
                )
            )
        }
    }

    private suspend fun retrievePartners(url: String): List<PartnerConfig> {
        return clientProvider.client.get(url).body()
    }

    fun shouldShareInterests(url: Url, type: UrlType): Boolean {
        val partnersSequence = serviceParams.value.partners.asSequence()
        val partner = when (type) {
            UrlType.RequestUrl -> partnersSequence.filter { it.requestHeader }
            UrlType.ResponseUrl -> partnersSequence.filter { it.responseHeader }
        }
            .filter {
                val protocol = it.url?.protocol
                protocol?.name == "any" || protocol?.name == url.protocol.name
            }
            .filter {
                val hostEnd = it.url?.host?.replace("*", "") ?: ""
                url.host.endsWith(hostEnd, ignoreCase = false)
            }
            .filter {
                var path = it.url?.encodedPath ?: ""
                var encodedPath = url.encodedPath
                if (path == "") path = "/"
                if (encodedPath == "") encodedPath = "/"
                when {
                    path == "*" -> true
                    path.contains("*") -> {
                        val regex = path.replace("*", ".*")
                        encodedPath.matches(regex.toRegex())
                    }
                    else -> encodedPath == path
                }
            }.firstOrNull()
        return partner != null
    }

    enum class UrlType {
        RequestUrl,
        ResponseUrl
    }

    companion object {
        private const val PARTNER_SERVICE_PARAMS_KEY = "crumbs_partner_service_params"
    }

    @Serializable
    internal data class ServiceParams(
        val partners: List<PartnerConfig> = listOf(),
        val lastUpdateTime: Long = 0L,
        val version: Int = 1,
    )

    @Serializable
    internal data class PartnerConfig(
        val pattern: String,
        val responseHeader: Boolean = false,
        val requestHeader: Boolean = false,
    ) {
        @Transient
        val url: Url? = pattern.replace("*://", "any://").toUrl()
    }
}