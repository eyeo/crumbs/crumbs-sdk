package org.crumbs.service

import com.eyeo.pat.PatLogger
import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.UpdatedStoredValue
import com.eyeo.pat.utils.SystemTimeProvider.Companion.getCurrentDateTimeFormatted
import com.eyeo.pat.utils.TimeProvider
import com.eyeo.privacy.models.PrivacySettings
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import org.crumbs.CoreBuildConfig
import org.crumbs.models.AppInfo
import org.crumbs.models.ProfileData
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random.Default.nextBoolean
import kotlin.random.Random.Default.nextDouble
import kotlin.random.Random.Default.nextLong

internal class ReportService internal constructor(
    private val coroutineContext: CoroutineContext,
    private val profileService: ProfileService,
    private val privacySettings: SettingsProvider<PrivacySettings>,
    private val clientProvider: HttpClientProvider,
    private val resourcesProvider: ResourcesProvider,
    private val appInfo: AppInfo,
    storageProvider: StorageProvider,
    timeProvider: TimeProvider,
) {
    private val logger = PatLogger("ReportService")
    private val params = UpdatedStoredValue(
        key = REPORT_SERVICE_PARAMS_KEY,
        storageProvider = storageProvider,
        timeProvider = timeProvider,
        serializer = ServiceParams.serializer(),
        defaultBuilder = { ServiceParams() },
        updateDelayMs = REPORT_INTERESTS_INTERVAL_MS
    )

    internal suspend fun runCron(): Long {
        return params.update()
        {
            withContext(coroutineContext) {
                val interests = getInterestsWithDifferentialPrivacy()
                withContext(Dispatchers.Default) {
                    val token =
                        resourcesProvider.decrypt("data/config.json", CoreBuildConfig.apiToken)

                    val profileData = profileService.getCrumbsProfile().data
                    val httpResponse = sendReport(
                        interests = interests,
                        appInfo = appInfo,
                        privacySettings = privacySettings.get(),
                        profile = profileData,
                        token = token,
                    )
                    logger.d("Report response: ${httpResponse.status}")
                    if (!httpResponse.status.isSuccess())
                        throw Exception("Can't send the report. Response: ${httpResponse.status}")
                }
                params.store(params.value.copy(user = User()))
            }
        }
    }

    internal suspend fun sendReport(
        interests: Set<Int>,
        appInfo: AppInfo,
        privacySettings: PrivacySettings,
        profile: ProfileData,
        token: String,
        url: String = REPORT_URL
    ): HttpResponse {
        return clientProvider.client.post(url) {
            contentType(ContentType.Application.Json)
            headers {
                header(HttpHeaders.Authorization, "Bearer $token")
            }
            setBody(
                ReportBody(
                    ReportPayload(
                        app = appInfo,
                        createdAt = getCurrentDateTimeFormatted(),
                        eventName = REPORT_EVENT_NAME,
                        eventParams = EventParams(
                            lmt = profile.lmt,
                            dnt = privacySettings.enableDoNotTrack,
                            gpc = privacySettings.enableGPC,
                            country = profile.location?.countryCode,
                            acceptLanguage = profile.acceptLanguage,
                            userAgent = profile.userAgent,
                            rapporMessage = interests,
                            rapporMessageVersion = REPORT_VERSION
                        ),
                        user = params.value.user
                    ),
                )
            )
        }
    }

    internal fun getInterestsWithDifferentialPrivacy(): Set<Int> {
        // 1. Signal
        // user interests, represented as an set of category ID the user is interested in.
        val signalInterests = profileService.getRelevantInterestIds().toMutableSet()

        // 2. Permanent randomized response
        // apply noise to the user interests
        params.value.noise.forEach {
            if (it.value) {
                signalInterests.add(it.key)
            } else {
                signalInterests.remove(it.key)
            }
        }

        // 3. Instantaneous randomized response
        // On this step we compute the final array of interests ids.
        // instantResponse is computed according to the formula:
        // Probability(instantResponse[i] = 1) = | RAPPOR_Q, if signalInterests[i] is true
        //                                       | RAPPOR_P, if signalInterests[i] is false
        val instantResponse = mutableSetOf<Int>()
        repeat(REPORT_NUM_CATEGORIES) { id ->
            val containsId = signalInterests.contains(id)
            if (
                (containsId && nextDouble() <= RAPPOR_Q) ||
                (!containsId && nextDouble() <= RAPPOR_P)
            ) {
                instantResponse.add(id)
            }
        }

        return instantResponse
    }

    companion object {
        private const val REPORT_NUM_CATEGORIES = 621
        private const val RAPPOR_F = 0.5
        private const val RAPPOR_Q = 0.75
        private const val RAPPOR_P = 0.5
        private const val REPORT_VERSION = "1.0"
        private const val REPORT_URL =
            "https://crumbs.telemetry.eyeo.com/topic/crumbs_event/version/4"
        private const val DAY_IN_MS = 24 * 60 * 60 * 1000L // 1d
        private const val REPORT_INTERESTS_INTERVAL_MS = DAY_IN_MS * 7

        private const val REPORT_EVENT_NAME = "privacy_preserving_interests"
        private const val REPORT_SERVICE_PARAMS_KEY = "crumbs_report_service_params"
    }

    @Serializable
    private data class ReportBody(val payload: ReportPayload)

    @Serializable
    private data class ReportPayload(
        val app: AppInfo?,
        val createdAt: String = getCurrentDateTimeFormatted(),
        val eventName: String,
        val eventParams: EventParams,
        val user: User
    )

    @Serializable
    private data class EventParams(
        val lmt: Boolean,
        val dnt: Boolean,
        val gpc: Boolean,
        val country: String?,
        val acceptLanguage: String?,
        val userAgent: String?,
        val rapporMessage: Set<Int>,
        val rapporMessageVersion: String,
    )

    @Serializable
    internal data class User(
        val id: String = "${genHex(8)}-${genHex(4)}-${genHex(4)}-${genHex(4)}-${genHex(12)}"
    ) {
        companion object {
            private fun genHex(size: Int) =
                ((1L shl size * 4) + nextLong(1L shl size * 4)).toString(16).substring(1)
        }
    }

    @Serializable
    private data class ServiceParams(
        val noise: Map<Int, Boolean> = generateNoise(),
        val version: Int = 1,
        val user: User = User(),
    ) {
        companion object {

            private fun generateNoise(): Map<Int, Boolean> {
                return hashMapOf<Int, Boolean>().apply {
                    repeat(REPORT_NUM_CATEGORIES) { id ->
                        if (nextDouble() <= RAPPOR_F) {
                            this[id] = nextBoolean()
                        }
                    }
                }
            }
        }
    }
}

