package org.crumbs.service

import com.eyeo.pat.PatLogger
import com.eyeo.pat.browser.plugins.TabPlugin
import com.eyeo.pat.browser.plugins.Visit
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.StoredValue
import com.eyeo.pat.provider.UpdatedStoredValue
import com.eyeo.pat.utils.TimeProvider
import kotlinx.coroutines.*
import kotlinx.serialization.Serializable
import org.crumbs.models.InterestsSettings
import org.crumbs.models.Page
import org.crumbs.models.VisitCount
import org.crumbs.models.VisitedPages
import org.crumbs.provider.MetaDataProvider
import kotlin.coroutines.CoroutineContext
import kotlin.math.absoluteValue
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

internal class InterestsService(
    private val settings: SettingsProvider<InterestsSettings>,
    private val metaDataProvider: MetaDataProvider,
    private val timeProvider: TimeProvider,
    private val coroutineContext: CoroutineContext,
    storageProvider: StorageProvider
) {

    init {
        resetDisabledDomainsThisSession()
    }

    private var saveJob: Job? = null
    private val logger = PatLogger("InterestsService")
    val visitedPages = StoredValue(
        key = VISITED_PAGES_KEY,
        storageProvider = storageProvider,
        serializer = VisitedPages.serializer(),
        defaultBuilder = { VisitedPages() },
        cacheValue = true
    )

    private val params = UpdatedStoredValue(
        key = INTERESTS_SERVICE_PARAMS_KEY,
        timeProvider = timeProvider,
        storageProvider = storageProvider,
        serializer = ServiceParams.serializer(),
        defaultBuilder = { ServiceParams() },
        cacheValue = true,
        updateDelayMs = PROCESSING_MIN_DELAY_MS,
        retryDelayMs = PROCESSING_RETRY_MS
    )

    fun onEvent(url: String, eventType: TabPlugin.Type): Job {
        return CoroutineScope(coroutineContext).launch {
            visitedPages.value.apply {
                var currentPage = pages[url]
                if (eventType == TabPlugin.Type.Loaded) {
                    currentPage =
                        currentPage?.takeIf { it.createTime > timeProvider.getCurrentTimeMs() - DAY_IN_MS }
                    if (currentPage == null) {
                        metaDataProvider.getDomainData(url)?.category?.let {
                            currentPage = Page(
                                category = it,
                                createTime = timeProvider.getCurrentTimeMs()
                            ).apply {
                                pages[url] = this
                            }
                        }
                    }
                }

                currentPage?.also { page ->
                    var attentionDuration = ATTENTION_DURATION_MS
                    when (eventType) {
                        TabPlugin.Type.MediaPlaying -> {
                            page.isAudio = true
                        }

                        TabPlugin.Type.MediaStopped -> {
                            page.isAudio = false
                            return@also // we don't count no audio has a interaction
                        }

                        TabPlugin.Type.Loaded -> {
                            attentionDuration = 1
                        }

                        else -> {
                        }
                    }

                    if (page.isAudio) {
                        ATTENTION_AUDIO_THRESHOLDS
                    } else {
                        ATTENTION_THRESHOLDS
                    }.getOrNull(page.score)?.let { threshold ->
                        val systemTimeInMillis = timeProvider.getCurrentTimeMs()
                        val timeDelta = systemTimeInMillis - page.lastInteraction
                        val duration = min(attentionDuration, timeDelta)
                        val newAttention = max(page.attention + duration, attentionDuration)
                        page.lastInteraction = systemTimeInMillis
                        page.attention = newAttention
                        if (newAttention.div(1000) >= threshold) {
                            page.score++
                            incrementInterest(page.category, 1)
                        }
                        logger.d(
                            "score: ${page.score} / " +
                                    "attention: ${page.attention.div(1000).toInt()} / " +
                                    "threshold: $threshold / url: $url"
                        )
                    }
                }
            }
            scheduleVisitedPagesSave()
        }
    }

    fun processedHistory() = params.value.historyAnalysed

    fun onHistoryReady(visits: List<Visit>) {
        if (!params.value.historyAnalysed) {
            if (params.value.historyAnalysed) {
                return
            }
            params.store(params.value.copy(historyAnalysed = true))
            CoroutineScope(coroutineContext).launch {
                visits.groupBy { it.url }
                    .map { entry ->
                        var lastVisit = -DAY_IN_MS
                        val visitsCount = entry.value.sortedBy { it.timestampMs }
                            .count {
                                if ((it.timestampMs - lastVisit).absoluteValue < DAY_IN_MS) {
                                    false
                                } else {
                                    lastVisit = it.timestampMs.toLong()
                                    true
                                }
                            }
                        VisitCount(entry.key, visitsCount)
                    }
                    .onEach { analyseVisit(it) }
            }
        }
    }

    fun setInterestState(category: Int, enable: Boolean) {
        settings.edit {
            val blockedInterests = it.blockedInterests.toMutableSet()
            if (enable) {
                blockedInterests.remove(category)
            } else {
                blockedInterests.add(category)
            }
            it.copy(blockedInterests = blockedInterests.toSet())
        }
    }

    fun resetInterest(interestId: Int) {
        settings.edit {
            val interests = it.interests.toMutableMap()
            interests.remove(interestId)
            it.copy(interests = interests)
        }
    }

    fun getSettings() = settings.get()

    fun listenSettings() = settings.listen()

    fun getSettingsProvider() = settings

    internal suspend fun runCron(): Long {
        return params.update {
            val currentTimeMs = timeProvider.getCurrentTimeMs()
            visitedPages.value.apply {
                val filterPages =
                    pages.filter { it.value.createTime > currentTimeMs - PROCESSING_MIN_DELAY_MS }
                pages.clear()
                pages.putAll(filterPages)
            }
            scheduleVisitedPagesSave()

            // decay interests
            val interests = settings.get().interests.toMutableMap()
            interests.forEach {
                interests[it.key] = floor(it.value * INTERESTS_DECAY_RATE)
            }
            settings.edit {
                it.copy(interests = interests.filterValues { value -> value > 0 })
            }
        }
    }

    private fun analyseVisit(visit: VisitCount) {
        val data = metaDataProvider.getDomainData(visit.url)
        data?.let {
            incrementInterest(data.category, visit.count)
        }
    }

    private fun incrementInterest(category: Int, value: Int) {
        settings.edit {
            val interests = it.interests.toMutableMap()
            val currentValue = interests.getOrElse(category, defaultValue = { 0f })
            val newValue = value + currentValue
            interests[category] = newValue
            logger.d("Update interest: $category = $newValue")
            it.copy(interests = interests)
        }
    }

    private suspend fun scheduleVisitedPagesSave() {
        coroutineScope {
            saveJob?.cancel()
            saveJob = launch(coroutineContext) {
                delay(300)
                if (isActive) {
                    val latestPages = visitedPages.value
                    visitedPages.store(latestPages)
                }
            }
        }
    }

    private fun resetDisabledDomainsThisSession() {
        settings.edit { settings -> settings.copy(disabledDomainsThisSession = setOf()) }
    }

    companion object {
        private const val VISITED_PAGES_KEY = "crumbs_visited_pages"
        private const val INTERESTS_SERVICE_PARAMS_KEY = "crumbs_insights_service_params"
        internal const val DAY_OF_HISTORY = 90
        internal const val ATTENTION_DURATION_MS = 15000L // 00:00:15
        private const val DAY_IN_MS = 24 * 60 * 60 * 1000L // 1d
        private const val PROCESSING_MIN_DELAY_MS = DAY_IN_MS
        private const val PROCESSING_RETRY_MS = 5 * 60 * 1000L // 5 minutes
        private const val INTERESTS_DECAY_RATE = 0.95f // 5% decay
        internal val ATTENTION_THRESHOLDS = arrayOf(
            0,
            50, // 00:00:50
            96, // 00:01:36
            138, // 00:02:18
            176, // 00:02:56
            210 // 00:03:30
        )
        internal val ATTENTION_AUDIO_THRESHOLDS = arrayOf(
            0,
            140, // 00h:02m:20s
            400, // 00h:06m:40s
            880, // 00h:14m:40s
            1480, // 00h:24m:40s
            2460, // 00h:41m:00s
            3660 // 01h:01m:00s
        )
    }

    @Serializable
    private data class ServiceParams(
        val historyAnalysed: Boolean = false,
    )
}
