package org.crumbs.models

import com.eyeo.pat.models.FeatureSettings
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class InterestsSettings(
    override val enabled: Boolean = false,
    @Suppress("NON_EXPORTABLE_TYPE")
    val interests: Map<Int, Float> = mapOf(),
    @Suppress("NON_EXPORTABLE_TYPE")
    val blockedInterests: Set<Int> = setOf(),
    /**
     * Domains where interests sharing is disabled
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val disabledDomains: Set<String> = setOf(),
    /**
     * Domains where interests sharing is disabled for this session
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val disabledDomainsThisSession: Set<String> = setOf(),
) : FeatureSettings {

    fun isSharingEnabledWith(url: String) =
        enabled && !disabledDomains.contains(url) && !disabledDomainsThisSession.contains(url)
}

