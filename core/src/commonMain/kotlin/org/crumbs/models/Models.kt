package org.crumbs.models

import io.ktor.http.HttpHeaders
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@Serializable
data class CrumbsDomainData(val category: Int)

@Serializable
data class AppInfo(val name: String, val version: String)

@Serializable
data class ProfileDeviceInfo(var platform: String, var os: String)

@Serializable
data class VisitedPages(
    val pages: HashMap<String, Page> = hashMapOf()
)

@Serializable
data class Page(
    var attention: Long = 0,
    var lastInteraction: Long = 0,
    var isAudio: Boolean = false,
    val category: Int,
    var score: Int = 0,
    val createTime: Long
)

data class VisitCount(var url: String, var count: Int)

enum class RequestContentType {
    OTHER, SCRIPT, IMAGE, STYLESHEET, OBJECT, SUBDOCUMENT, DOCUMENT, WEBSOCKET,
    WEBRTC, PING, XMLHTTPREQUEST, OBJECT_SUBREQUEST, MEDIA, FONT, GENERICBLOCK,
    ELEMHIDE, GENERICHIDE
}

@Serializable
@JsExport
data class CrumbsInterest(
    val id: Int,
    val name: String,
    var value: String? = null,
    val parentId: Int?,
    val emoji: String = ""
)

object CrumbsHeaders {
    val CacheControl: String = HttpHeaders.CacheControl
    const val XCrumbsCountryCode: String = "X-Crumbs-Country-Code"
    const val XCrumbsInterests: String = "x-crumbs-interests"
}
