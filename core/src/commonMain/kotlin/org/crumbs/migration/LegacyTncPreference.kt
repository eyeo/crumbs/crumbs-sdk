package org.crumbs.migration

import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.StoredValue
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.TermsAndConditionsState
import kotlinx.serialization.builtins.serializer

internal class LegacyTncPreference constructor(
    storageProvider: StorageProvider) {
    private val sharedPref = StoredValue(TERMS_AND_CONDITIONS_KEY,
        storageProvider,
        Int.serializer(),
        { TermsAndConditionsState.NONE.ordinal },
        version = 2,
        onUpgrade = { oldVersion, _ ->
            if (oldVersion == 1) {
                val privacySettings = SettingsProvider(
                    LegacyPrivacySettings.SETTINGS_KEY,
                    storageProvider,
                    PrivacySettings.serializer(),
                    defaultBuilder = { PrivacySettings() }
                )
                if (privacySettings.get().enabled) {
                    TermsAndConditionsState.ACCEPTED.ordinal
                } else {
                    TermsAndConditionsState.NONE.ordinal
                }
            } else {
                null
            }
        })

    fun setTermsAndConditionsState(state: TermsAndConditionsState) {
        sharedPref.store(state.ordinal)
    }

    fun getTermsAndConditionsState() = TermsAndConditionsState.values()[sharedPref.value]

    companion object {
        internal const val TERMS_AND_CONDITIONS_KEY = "org.crumbs.ui.tnc.TERMS_AND_CONDITION_KEY"
        internal const val TERMS_AND_CONDITION_KEY_VERSION =
            "${TERMS_AND_CONDITIONS_KEY}_storage_version"
    }
}