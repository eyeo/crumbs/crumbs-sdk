package org.crumbs.utils

import org.crumbs.models.RequestContentType

class ContentTypeDetector {
    /**
     * Detects ContentType for given URL
     * @param url URL
     * @return RequestContentType or `RequestContentType.OTHER` if not detected
     */
    fun detect(url: String): RequestContentType {
        return when {
            RE_JS.matches(url) -> {
                RequestContentType.SCRIPT
            }
            RE_CSS.matches(url) -> {
                RequestContentType.STYLESHEET
            }
            RE_IMAGE.matches(url) -> {
                RequestContentType.IMAGE
            }
            RE_FONT.matches(url) -> {
                RequestContentType.FONT
            }
            RE_HTML.matches(url) -> {
                RequestContentType.SUBDOCUMENT
            }
            else -> {
                RequestContentType.OTHER
            }
        }
    }

    companion object {
        private val RE_JS = Regex("\\.js(?:\\?.+)?", RegexOption.IGNORE_CASE)
        private val RE_CSS = Regex("\\.css(?:\\?.+)?", RegexOption.IGNORE_CASE)
        private val RE_IMAGE = Regex("\\.(?:gif|png|jpe?g|bmp|ico)(?:\\?.+)?", RegexOption.IGNORE_CASE)
        private val RE_FONT = Regex("\\.(?:ttf|woff)(?:\\?.+)?", RegexOption.IGNORE_CASE)
        private val RE_HTML = Regex("\\.html?(?:\\?.+)?", RegexOption.IGNORE_CASE)
    }
}