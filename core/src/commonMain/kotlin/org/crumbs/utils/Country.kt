package org.crumbs.utils

/**
 * Given a country code, returns the corresponding country name
 * @param code the country code in ISO 3166-1 alpha-2 format
 */
expect fun codeToCountryName(code: String) : String