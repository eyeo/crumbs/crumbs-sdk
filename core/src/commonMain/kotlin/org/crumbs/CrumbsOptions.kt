package org.crumbs

import com.eyeo.privacy.PrivacyShieldOptions
import kotlin.js.JsExport
import kotlin.jvm.JvmOverloads

@JsExport
data class CrumbsOptions @JvmOverloads constructor(
    var partnersListUrl: String = "https://crumbs.org/partners-v2.json",
    var safeToShareListUrl: String = "https://easylist-downloads.adblockplus.org/crumbs/crumbs_abb.json",
    var privacyShieldOptions: PrivacyShieldOptions = PrivacyShieldOptions(),
)