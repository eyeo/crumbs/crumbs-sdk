package org.crumbs.provider

import org.crumbs.models.AppInfo
import org.crumbs.models.ProfileDeviceInfo
import org.w3c.dom.Navigator

internal class JsBrowserInfoProvider(private val appInfo: AppInfo) : PlatformInfoProvider {
    override suspend fun isLimitAdTrackingEnabled(): Boolean {
        return false
    }

    override fun getPlatformLanguages(): String {
        return navigator.languages.joinToString { ", " }
    }

    override fun getDeviceInfo(): ProfileDeviceInfo {
        return ProfileDeviceInfo("web-extension", navigator.platform)
    }

    override fun getAppInfo(): AppInfo = appInfo

}

external val navigator: Navigator
