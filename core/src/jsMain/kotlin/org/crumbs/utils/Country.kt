package org.crumbs.utils


actual fun codeToCountryName(code: String): String {
    val names = js("new Intl.DisplayNames([], { type: 'region', fallback: 'code'})")
    return names.of(code) as String
}