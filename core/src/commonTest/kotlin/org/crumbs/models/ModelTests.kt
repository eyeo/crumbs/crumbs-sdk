package org.crumbs.models

import kotlin.test.Test
import kotlin.test.assertEquals

class ModelTests {

    @Test
    fun profileLocationTest(){
        val location = ProfileLocation("Country Name", "Country Code")
        assertEquals("Country Name", location.toString())
    }

}