package org.crumbs.service

import com.eyeo.pat.browser.BrowserIntegration
import com.eyeo.pat.browser.BrowserPluginApi
import com.eyeo.pat.browser.plugins.*
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.Coroutines.getSingleThreadContext
import com.eyeo.pat.utils.TimeProvider
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import com.russhwolf.settings.Settings
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.crumbs.models.CrumbsDomainData
import org.crumbs.models.CrumbsInterest
import org.crumbs.models.InterestsSettings
import org.crumbs.provider.*
import org.crumbs.service.InterestsService.Companion.ATTENTION_AUDIO_THRESHOLDS
import org.crumbs.service.InterestsService.Companion.ATTENTION_DURATION_MS
import org.crumbs.service.InterestsService.Companion.ATTENTION_THRESHOLDS
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext
import kotlin.test.assertEquals

class InterestsServiceTests : TimeProvider {
    private val coroutineContext: CoroutineContext = getSingleThreadContext()

    private lateinit var interestsService: InterestsService

    @RelaxedMockK
    lateinit var dataProvider: MetaDataProvider

    @RelaxedMockK
    lateinit var storageSettings: Settings

    @RelaxedMockK
    lateinit var settingsProvider: SettingsProvider<InterestsSettings>

    var timeMs: Long = getCurrentTimeMs()

    private var settings: InterestsSettings? = null

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        coEvery { dataProvider.getDomainData(not(match { it.contains(TEST_DOMAIN.getUrlHost()!!) })) }.returns(
            null
        )
        coEvery { dataProvider.getDomainData(or(TEST_DOMAIN, TEST_DOMAIN.getUrlHost()!!)) }.returns(
            CrumbsDomainData(
                TEST_INTEREST_ID
            )
        )
        coEvery { dataProvider.getInterest(TEST_INTEREST_ID) }.returns(
            CrumbsInterest(
                1,
                "TestCategory",
                null,
                null,
                "test"
            )
        )
        settings = InterestsSettings()
        every { settingsProvider.get() }.answers {
            settings!!
        }
        every { settingsProvider.save(any()) }.answers {
            settings = it.invocation.args.first() as InterestsSettings?
        }

        every { settingsProvider.edit(any()) }.answers {
            @Suppress("UNCHECKED_CAST") val callback =
                it.invocation.args.first() as ((settings: InterestsSettings) -> InterestsSettings)
            settings = callback.invoke(settings!!)
        }
        interestsService =
            InterestsService(
                settingsProvider,
                dataProvider,
                this,
                coroutineContext,
                StorageProvider(storageSettings)
            )
    }

    @Test
    fun testUnknownWebsite() = runBlocking {
        interestsService.onEvent("https://unknwon.org", TabPlugin.Type.Loaded).join()
        assert(interestsService.getSettings().interests.isEmpty())
        assert(interestsService.visitedPages.value.pages.isEmpty())
    }

    @Test
    fun testKnowWebsite() = runBlocking {
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        assert(interestsService.getSettings().interests.isNotEmpty())
        assert(interestsService.getSettings().interests.containsKey(TEST_INTEREST_ID))
        assert(interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score == 1)
    }

    @Test
    fun testPageTimeout() = runBlocking {
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        assert(interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score == 1)
        repeat(
            kotlin.math.ceil(ATTENTION_THRESHOLDS[1] * 1000 / ATTENTION_DURATION_MS.toDouble())
                .toInt()
        ) {
            interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Interact).join()
            timeMs += ATTENTION_DURATION_MS
        }
        assertEquals(2, interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score)
        timeMs += TimeUnit.DAYS.toMillis(1)
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        assert(interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score == 1)
        assert(3 == interestsService.getSettings().interests[TEST_INTEREST_ID]?.toInt())
    }

    @Test
    fun testAudioThresholds() = runBlocking {
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.MediaPlaying).join()
        repeat(
            kotlin.math.ceil(ATTENTION_THRESHOLDS[1] * 1000 / ATTENTION_DURATION_MS.toDouble())
                .toInt()
        ) {
            interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Interact).join()
            timeMs += ATTENTION_DURATION_MS
        }
        assert(interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score == 1)

        repeat(
            kotlin.math.ceil(ATTENTION_AUDIO_THRESHOLDS[1] * 1000 / ATTENTION_DURATION_MS.toDouble())
                .toInt()
        ) {
            interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Interact).join()
            timeMs += ATTENTION_DURATION_MS
        }
        assert(interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score == 2)
    }

    @Test
    fun testMaxThresholds() = runBlocking {
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        repeat(
            kotlin.math.ceil(ATTENTION_THRESHOLDS.last() * 1000 / ATTENTION_DURATION_MS.toDouble())
                .toInt()
        ) {
            interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Interact).join()
            timeMs += ATTENTION_DURATION_MS
        }
        println(interestsService.visitedPages.value.pages[TEST_DOMAIN])
        assertEquals(
            ATTENTION_THRESHOLDS.size,
            interestsService.visitedPages.value.pages[TEST_DOMAIN]?.score
        )
    }

    @Test
    fun testAttentionDebounce() = runBlocking {
        interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Loaded).join()
        repeat(2) {
            interestsService.onEvent(TEST_DOMAIN, TabPlugin.Type.Interact).join()
        }
        println(interestsService.visitedPages.value.pages[TEST_DOMAIN])
        assertEquals(
            ATTENTION_DURATION_MS,
            interestsService.visitedPages.value.pages[TEST_DOMAIN]?.attention
        )
    }

    @Test
    fun testHistoryProvider() = runBlocking {
        BrowserIntegration.History.setHistoryProvider(
            object : HistoryProvider {
                override fun queryHistory(numberOfDays: Int, callback: HistoryListener) {
                    val visits = arrayListOf(
                        Visit(TEST_DOMAIN, getCurrentTimeMs().toDouble()),
                        Visit(TEST_DOMAIN, getCurrentTimeMs().toDouble()),
                        Visit(TEST_DOMAIN, getCurrentTimeMs().toDouble() + TimeUnit.DAYS.toMillis(1)))
                    callback.onHistoryReady(visits.toTypedArray())
                }
            }
        )
        BrowserPluginApi.History.queryHistory(5) {
            interestsService.onHistoryReady(it.toList())
        }
        delay(200)
        assertEquals(2, interestsService.getSettings().interests[TEST_INTEREST_ID]?.toInt())
    }

    companion object {
        const val TEST_DOMAIN = "https://mytestdomain.com"
        const val TEST_INTEREST_ID = 1
    }

    override fun getCurrentTimeMs(): Long = timeMs
}