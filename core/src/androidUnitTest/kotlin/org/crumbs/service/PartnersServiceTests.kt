package org.crumbs.service

import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.SystemTimeProvider
import io.ktor.http.Url
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.serialization.builtins.serializer
import org.crumbs.models.InterestsSettings
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PartnersServiceTests {

    @RelaxedMockK
    private lateinit var clientProvider: HttpClientProvider

    @RelaxedMockK
    private lateinit var storageProvider: StorageProvider

    private lateinit var partnersService: PartnersService

    private val partners = ArrayList<PartnersService.PartnerConfig>()

    private val params = PartnersService.ServiceParams(
        partners
    )

    private lateinit var interestsSettings: InterestsSettings


    @Before
    fun setUp() {
        interestsSettings = InterestsSettings()
        MockKAnnotations.init(this, relaxUnitFun = true)
        coEvery {
            storageProvider.load(
                any(),
                PartnersService.ServiceParams.serializer(),
                any()
            )
        }.answers { params }
        coEvery {
            storageProvider.load(
                any(),
                Int.serializer(),
                any()
            )
        }.answers { 1 }
        partners.clear()
        partnersService = PartnersService(
            SystemTimeProvider(),
            clientProvider,
            storageProvider,
            "fakeUrl"
        )
    }

    @ExperimentalStdlibApi
    @Test
    fun testUrlMatching() {

        partners.clear()
        partners.add(GOOGLE_REQUEST_CONFIG)

        assertTrue(
            partnersService.shouldShareInterests(Url(GOOGLE_URL), PartnersService.UrlType.RequestUrl),
            "Should share interests on $GOOGLE_URL"
        )
        assertFalse(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should not share interests on $GOOGLE_URL"
        )

        partners.clear()
        partners.add(GOOGLE_RESPONSE_CONFIG)
        assertFalse(
            partnersService.shouldShareInterests(Url(GOOGLE_URL), PartnersService.UrlType.RequestUrl),
            "Should not share interests on $GOOGLE_URL"
        )
        assertTrue(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests on $GOOGLE_URL"
        )

        partners.clear()
        partners.add(GOOGLE_REQUEST_CONFIG)
        partners.add(GOOGLE_RESPONSE_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(Url(GOOGLE_URL), PartnersService.UrlType.RequestUrl),
            "Should share interests on $GOOGLE_URL"
        )
        assertTrue(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests on $GOOGLE_URL"
        )

        partners.clear()
        partners.add(WILDCARD_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests on $GOOGLE_URL"
        )
        assertFalse(
            partnersService.shouldShareInterests(
                Url("ftp://www.google.com/"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests with ftp protocol"
        )

        partners.clear()
        partners.add(PATH_WILDCARD_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url("https://example.com/foo/bar.html"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )
        assertFalse(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests with ftp protocol"
        )
        partners.clear()
        partners.add(SUBDOMAIN_WILDCARD_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url("https://docs.google.com/foobar"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )
        assertTrue(
            partnersService.shouldShareInterests(
                Url("https://www.google.com/foo/baz/bar?test=ok"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )

        partners.clear()
        partners.add(PROTOCOL_WILDCARD_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url(GOOGLE_URL),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests on $GOOGLE_URL"
        )
        assertTrue(
            partnersService.shouldShareInterests(
                Url("ftp://www.google.com/"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests with ftp protocol"
        )

        partners.clear()
        partners.add(FILE_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url("file:///foo/bar.html"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )

        partners.clear()
        partners.add(HTTP_CONFIG)
        assertTrue(
            partnersService.shouldShareInterests(
                Url("http://127.0.0.1/"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )
        assertTrue(
            partnersService.shouldShareInterests(
                Url("http://127.0.0.1/foo/bar.html"),
                PartnersService.UrlType.ResponseUrl
            ),
            "Should share interests"
        )
    }

    companion object {
        private const val GOOGLE_URL = "https://www.google.com/"
        private val GOOGLE_REQUEST_CONFIG = PartnersService.PartnerConfig(
            GOOGLE_URL, responseHeader = false,
            requestHeader = true,
        )
        private val GOOGLE_RESPONSE_CONFIG = PartnersService.PartnerConfig(
            GOOGLE_URL, responseHeader = true,
            requestHeader = false,
        )

        private val WILDCARD_CONFIG = PartnersService.PartnerConfig(
            "https://*/*", responseHeader = true,
            requestHeader = true,
        )
        private val PATH_WILDCARD_CONFIG = PartnersService.PartnerConfig(
            "https://*/foo*", responseHeader = true,
            requestHeader = true,
        )
        private val SUBDOMAIN_WILDCARD_CONFIG = PartnersService.PartnerConfig(
            "https://*.google.com/foo*bar", responseHeader = true,
            requestHeader = true,
        )
        private val PROTOCOL_WILDCARD_CONFIG = PartnersService.PartnerConfig(
            "*://www.google.com/", responseHeader = true,
            requestHeader = true,
        )
        private val FILE_CONFIG = PartnersService.PartnerConfig(
            "file:///foo*", responseHeader = true,
            requestHeader = true,
        )
        private val HTTP_CONFIG = PartnersService.PartnerConfig(
            "http://127.0.0.1/*", responseHeader = true,
            requestHeader = true,
        )
    }


}