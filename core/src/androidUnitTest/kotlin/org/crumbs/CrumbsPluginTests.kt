package org.crumbs

import com.eyeo.pat.browser.BrowserIntegration
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.PrivacyShieldHeaders
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import kotlinx.coroutines.*
import kotlinx.coroutines.test.setMain
import org.crumbs.models.CrumbsHeaders
import org.crumbs.models.InterestsSettings
import org.crumbs.models.ShareableProfile
import org.crumbs.provider.ProfileDataProvider
import org.crumbs.service.InterestsService
import org.crumbs.service.PartnersService
import org.crumbs.service.ProfileService
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.Executors
import kotlin.test.assertTrue

@OptIn(DelicateCoroutinesApi::class)
class CrumbsPluginTests {

    private lateinit var plugin: CrumbsPlugin

    @RelaxedMockK
    private lateinit var interestsService: InterestsService

    @RelaxedMockK
    private lateinit var partnersService: PartnersService

    @RelaxedMockK
    private lateinit var profileService: ProfileService

    @MockK
    private lateinit var profileDataProvider: ProfileDataProvider

    @RelaxedMockK
    private var interestsSettings = InterestsSettings()

    @RelaxedMockK
    private lateinit var privacySettings: SettingsProvider<PrivacySettings>

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Default)

        MockKAnnotations.init(this, relaxUnitFun = true)

        every { interestsService.getSettings() }.answers { interestsSettings }
        every { profileService.profile }.answers { profileDataProvider }

        plugin = CrumbsPlugin(
            privacySettings = privacySettings,
            interestsService = interestsService,
            partnersService = partnersService,
            profileService = profileService,
        )

        runBlocking {
            GlobalScope.launch(Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
                plugin.setup()
            }
        }
    }

    @After
    fun release() {
        interestsSettings = InterestsSettings()
        plugin.destroy()
    }

    @Test
    fun testSetUserAgentAndLanguages() {
        val dummyUserAgent = "dummy user agent"
        val dummyLanguage = "dummy language"

        val headers = listOf(
            PrivacyShieldHeaders.UserAgent to dummyUserAgent,
            PrivacyShieldHeaders.AcceptLanguage to dummyLanguage
        )

        val url = "www.google.com"

        BrowserIntegration.Headers.rewriteRequestHeaders(
            tabId = "1",
            documentUrl = url,
            requestUrl = url,
            requestType = RequestType.MAIN_FRAME,
            incognito = false,
            headers = headers
        )

        verify { profileDataProvider.setUserAgent(dummyUserAgent) }
        verify { profileDataProvider.setLanguages(dummyLanguage) }
    }

    @Test
    fun testShouldAddInterestsHeader() {
        every { interestsSettings.isSharingEnabledWith(any()) }.answers { true }
        every { partnersService.shouldShareInterests(any(), any()) }.answers { true }

        every { privacySettings.get().enableDoNotTrack }.answers { true }

        val dummyProfile = ShareableProfile("RO", listOf(1, 2, 3))
        val expectedInterestsHeader = CrumbsPlugin.CrumbsInterestsHeader(dummyProfile)

        every {
            profileService.getShareableProfile(any(), any(), any())
        }.answers { dummyProfile }

        val url = "www.google.com"

        var result = BrowserIntegration.Headers.rewriteRequestHeaders(
            tabId = "1",
            documentUrl = url,
            requestUrl = url,
            requestType = RequestType.MAIN_FRAME,
            incognito = false,
            headers = listOf()
        )

        assertTrue(result.contains(expectedInterestsHeader.getName() to expectedInterestsHeader.getValue()))
        assertTrue(result.contains(CrumbsHeaders.CacheControl to "no-store"))

        result = BrowserIntegration.Headers.rewriteResponseHeaders(
            tabId = "1",
            documentUrl = url,
            requestUrl = url,
            requestType = RequestType.MAIN_FRAME,
            incognito = false,
            headers = listOf()
        )

        assertTrue(result.contains(expectedInterestsHeader.getName() to expectedInterestsHeader.getValue()))
        assertTrue(result.contains(CrumbsHeaders.CacheControl to "no-store"))
    }

    @Test
    fun testShouldNotAddInterestsHeader() {
        every { interestsSettings.isSharingEnabledWith(any()) }.answers { false }
        every { partnersService.shouldShareInterests(any(), any()) }.answers { true }
        every { privacySettings.get().enableDoNotTrack }.answers { true }

        val url = "www.google.com"

        var result = BrowserIntegration.Headers.rewriteRequestHeaders(
            tabId = "1",
            documentUrl = url,
            requestUrl = url,
            requestType = RequestType.MAIN_FRAME,
            incognito = false,
            headers = listOf()
        )

        assertTrue(result.isEmpty())

        result = BrowserIntegration.Headers.rewriteResponseHeaders(
            tabId = "1",
            documentUrl = url,
            requestUrl = url,
            requestType = RequestType.MAIN_FRAME,
            incognito = false,
            headers = listOf()
        )

        assertTrue(result.isEmpty())
    }
}