package org.crumbs.service

import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.TimeProvider
import com.russhwolf.settings.Settings
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.crumbs.models.AppInfo
import org.crumbs.provider.*
import org.crumbs.service.SafeToShareService.Companion.SAFE_TO_SHARE_LIST_KEY
import org.junit.Before
import org.junit.Test

internal class SafeToShareServiceTest {

    private lateinit var safeToShareService: SafeToShareService

    @MockK
    lateinit var storageSettings: Settings

    @RelaxedMockK
    lateinit var timeProvider: TimeProvider

    @RelaxedMockK
    lateinit var profileDataProvider: ProfileDataProvider

    @RelaxedMockK
    lateinit var httpClientProvider: HttpClientProvider

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        safeToShareService =
            SafeToShareService(
                StorageProvider(storageSettings),
                timeProvider,
                httpClientProvider,
                profileDataProvider,
                "fakeUrl",
                AppInfo("", ""),
            )
    }

    @Test
    fun testGetSafeToShareList() = runBlocking {
        val expectedSafeToShareList =
            SafeToShareList(
                listOf(42),
                listOf(MetaData("Dummy", "BR", false, "pt-BR, pt"))
            )
        val metaData = expectedSafeToShareList.metadata[0]
        val responseContent =
            """
            {
              "interests": [
                ${expectedSafeToShareList.interests[0]}
              ],
              "metadata": [
                {
                  "ua": "${metaData.ua}",
                  "country": "${metaData.country}",
                  "dnt": ${metaData.dnt},
                  "lang": "${metaData.lang}"
                }
              ]
            }
            """
        val expectedSafeToShareListData = SafeToShareListData(expectedSafeToShareList, "", 1)

        every { httpClientProvider.client } returns HttpClient(MockEngine) {
            followRedirects = true
            expectSuccess = true
            install(ContentNegotiation) {
                json()
            }
            install(Logging) {
                logger = Logger.SIMPLE
                level = LogLevel.ALL
            }
            engine {
                addHandler {
                    respond(
                        content = responseContent,
                        status = HttpStatusCode.OK,
                        headers = headersOf(HttpHeaders.ContentType, "application/json")
                    )
                }
            }
        }

        val updateTime = timeProvider.getCurrentTimeMs()
        every { timeProvider.getCurrentTimeMs() } returns (updateTime)

        launch { safeToShareService.runCron() }.join()

        verify(exactly = 1) {
            storageSettings.getStringOrNull(SAFE_TO_SHARE_LIST_KEY)
        }

        // update value
        verify(exactly = 1) {
            storageSettings.putString(
                SAFE_TO_SHARE_LIST_KEY,
                Json.encodeToString(SafeToShareListData.serializer(), expectedSafeToShareListData)
            )
        }

        // update time of update
        verify(exactly = 1) {
            storageSettings.putString(
                "${SAFE_TO_SHARE_LIST_KEY}_update_key",
                match { it.toLong() >= updateTime }
            )
        }
    }
}