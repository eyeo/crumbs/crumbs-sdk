package org.crumbs.service

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.Coroutines
import com.eyeo.pat.utils.SystemTimeProvider
import com.russhwolf.settings.Settings
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.crumbs.provider.MetaDataProvider
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.coroutines.CoroutineContext

@RunWith(AndroidJUnit4::class)
class AndroidDomainsServiceTest {
    private val coroutineContext: CoroutineContext = Coroutines.getSingleThreadContext()
    private lateinit var domainsService: MetaDataProvider

    @RelaxedMockK
    lateinit var storageSettings: Settings

    companion object {
        const val testHostname = "amazon.com"
        const val testCategory = "295"
    }

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        domainsService = MetaDataProvider(
            coroutineContext,
            HttpClientProvider(),
            StorageProvider(storageSettings),
            SystemTimeProvider()
        )
        runBlocking {
            domainsService.runCron()
        }
    }

    @Test
    fun testDomainsLoad() = runBlocking {
        val domainData = domainsService.getDomainData("http://$testHostname")
        assertEquals(testCategory, domainData?.category.toString())

    }

    @Test
    fun testDomainUrlEscape() = runBlocking {
        val domainData = domainsService.getDomainData("http://www.$testHostname")
        assertEquals(testCategory, domainData?.category.toString())
        assertNull(domainsService.getDomainData("http://${testHostname}www."))
        assertNotNull(domainsService.getDomainData("https://$testHostname"))
        assertNull(domainsService.getDomainData("this is not.a.domain"))
    }
}