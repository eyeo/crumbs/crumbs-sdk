package org.crumbs.service

import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.AndroidResourceProvider
import com.eyeo.pat.provider.HttpClientProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.Coroutines
import com.eyeo.pat.utils.SystemTimeProvider
import com.eyeo.privacy.models.PrivacySettings
import com.russhwolf.settings.Settings
import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondError
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.plugins.logging.SIMPLE
import io.ktor.content.TextContent
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.kotlinx.json.json
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.crumbs.CoreBuildConfig
import org.crumbs.models.AppInfo
import org.crumbs.models.ProfileData
import org.junit.Before
import org.junit.Test
import kotlin.coroutines.CoroutineContext
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class ReportServiceTests {
    private val coroutineContext: CoroutineContext = Coroutines.getSingleThreadContext()

    private lateinit var reportService: ReportService

    @RelaxedMockK
    internal lateinit var profileService: ProfileService

    @RelaxedMockK
    private lateinit var privacySettings: SettingsProvider<PrivacySettings>

    @RelaxedMockK
    lateinit var storageSettings: Settings

    @RelaxedMockK
    lateinit var httpClientProvider: HttpClientProvider

    private val resourcesProvider =
        AndroidResourceProvider(InstrumentationRegistry.getInstrumentation().targetContext)

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        every { profileService.getRelevantInterestIds() }.returns(emptySet())
        every { privacySettings.get() }.returns(PrivacySettings())

        reportService =
            ReportService(
                coroutineContext,
                profileService,
                privacySettings,
                httpClientProvider,
                resourcesProvider,
                AppInfo("", ""),
                StorageProvider(storageSettings),
                SystemTimeProvider(),
            )
    }

    @Test
    fun testInterestsWithDifferentialPrivacy() {
        val interest = reportService.getInterestsWithDifferentialPrivacy()
        assert(interest.isNotEmpty())
    }

    @Test
    fun testUserId() {
        val user = ReportService.User()
        assertEquals(user.id.length, "dae99296-645c-4b3a-7005-616a2eab3280".length)
        assertNotEquals(user.id, ReportService.User().id)
    }

    @Test
    fun testSendReport() = runBlocking {
        every { httpClientProvider.client }.answers {
            val client = HttpClient(MockEngine) {
                followRedirects = true
                expectSuccess = true
                install(ContentNegotiation) {
                    json()
                }
                install(Logging) {
                    logger = Logger.SIMPLE
                    level = LogLevel.ALL
                }
                engine {
                    addHandler { request ->
                        if (request.url.toString() == TEST_REPORT_URL) {
                            val body = request.body as TextContent
                            if (body.text.contains("privacy_preserving_interests")) {
                                return@addHandler respond("", HttpStatusCode.Created)
                            }
                        }

                        respondError(
                            HttpStatusCode.NotImplemented,
                            "no mock for ${request.url}"
                        )
                    }
                }

            }
            client
        }

        val token = resourcesProvider.decrypt("data/config.json", CoreBuildConfig.apiTestToken)
        val httpMock = reportService.sendReport(
            setOf(1, 5, 10),
            AppInfo("crumbs_core_test", CoreBuildConfig.version),
            PrivacySettings(),
            ProfileData(acceptLanguage = "en-US"),
            token = token,
            url = TEST_REPORT_URL
        )
        assertEquals(httpMock.status, HttpStatusCode.Created)
    }

    companion object {
        private const val TAG = "ReportServiceTests"
        private const val TEST_REPORT_URL =
            "https://test-telemetry.data.eyeo.it/topic/crumbs_event/version/4"
    }
}