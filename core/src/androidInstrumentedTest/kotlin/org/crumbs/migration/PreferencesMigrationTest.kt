package org.crumbs.migration

import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.TncPreference
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.PrivacyShieldStatsData
import com.eyeo.privacy.models.TermsAndConditionsState
import com.russhwolf.settings.Settings
import com.russhwolf.settings.SharedPreferencesSettings
import kotlinx.serialization.builtins.serializer
import org.crumbs.CrumbsAndroid
import org.junit.After
import org.junit.Before
import kotlin.test.Test

class PreferencesMigrationTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    private lateinit var crumbsSettings: Settings
    private lateinit var privacyShieldSettings: Settings

    private lateinit var crumbsStorageProvider: StorageProvider
    private lateinit var privacyShieldStorageProvider: StorageProvider

    private lateinit var crumbsSettingsProvider: SettingsProvider<LegacyPrivacySettings>
    private lateinit var privacyShieldSettingsProvider: SettingsProvider<PrivacySettings>

    @Before
    fun init() {
        privacyShieldSettings = SharedPreferencesSettings.Factory(context)
            .create(PrivacyShieldAndroid.SETTINGS_NAME).also { it.clear() }
        crumbsSettings = SharedPreferencesSettings.Factory(context)
            .create(CrumbsAndroid.SETTINGS_NAME).also { it.clear() }

        crumbsStorageProvider = StorageProvider(crumbsSettings)
        crumbsSettingsProvider = SettingsProvider(
            LegacyPrivacySettings.SETTINGS_KEY,
            crumbsStorageProvider,
            LegacyPrivacySettings.serializer(),
            defaultBuilder = { LegacyPrivacySettings() }
        )

        privacyShieldStorageProvider = StorageProvider(privacyShieldSettings)
        privacyShieldSettingsProvider = SettingsProvider(
            PreferencesMigration.PRIVACY_SHIELD_SETTINGS_KEY,
            privacyShieldStorageProvider,
            PrivacySettings.serializer(),
            defaultBuilder = { PrivacySettings() }
        )
    }

    @After
    fun release() {
        crumbsSettings.clear()
        privacyShieldSettings.clear()
    }

    @Test
    fun testMigrationFresh() {
        PreferencesMigration.migratePreferences(crumbsSettings, privacyShieldSettings)
        assert(crumbsSettings.keys.isEmpty())
        assert(privacyShieldSettings.keys.isEmpty())
    }

    @Test
    fun testMigrationFromLegacy() {
        val expectedPrivacySettings = LegacyPrivacySettings(
            enabled = false,
            blockHyperlinkAuditing = true,
            deAMP = true,
            enableFingerprintShield = false
        )
        val expectedStats = PrivacyShieldStatsData(11, 22, 33)
        val expectedTncState = TermsAndConditionsState.LATER

        crumbsStorageProvider.save(
            LegacyPrivacySettings.SETTINGS_KEY,
            expectedPrivacySettings,
            LegacyPrivacySettings.serializer()
        )
        assert(crumbsSettings.keys.contains(LegacyPrivacySettings.SETTINGS_KEY))

        crumbsStorageProvider.save(
            PreferencesMigration.LEGACY_GLOBAL_STATS_KEY,
            expectedStats,
            PrivacyShieldStatsData.serializer()
        )
        assert(crumbsSettings.keys.contains(PreferencesMigration.LEGACY_GLOBAL_STATS_KEY))

        val legacyTncPreference = LegacyTncPreference(crumbsStorageProvider)
        legacyTncPreference.setTermsAndConditionsState(expectedTncState)
        assert(crumbsSettings.keys.contains(LegacyTncPreference.TERMS_AND_CONDITIONS_KEY))

        PreferencesMigration.migratePreferences(crumbsSettings, privacyShieldSettings)

        assert(crumbsSettings.keys.isEmpty())
        assert(privacyShieldSettingsProvider.get().enabled == expectedPrivacySettings.enabled)
        assert(privacyShieldSettingsProvider.get().blockHyperlinkAuditing == expectedPrivacySettings.blockHyperlinkAuditing)
        assert(privacyShieldSettingsProvider.get().deAMP == expectedPrivacySettings.deAMP)
        assert(privacyShieldSettingsProvider.get().enableFingerprintShield == expectedPrivacySettings.enableFingerprintShield)

        val stats = privacyShieldStorageProvider.load(
            PreferencesMigration.GLOBAL_STATS_KEY,
            PrivacyShieldStatsData.serializer()
        )
        assert(stats == expectedStats)

        val tncState = privacyShieldStorageProvider.load(
            TncPreference.TERMS_AND_CONDITIONS_KEY,
            Int.serializer()
        )
        assert(tncState == expectedTncState.ordinal)
    }
}