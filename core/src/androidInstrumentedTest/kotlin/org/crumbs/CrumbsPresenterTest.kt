package org.crumbs

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.AndroidResourceProvider
import com.russhwolf.settings.SharedPreferencesSettings
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.crumbs.provider.AndroidInfoProvider
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertNotNull

@Suppress("unused")
@RunWith(AndroidJUnit4::class)
class CrumbsPresenterTest {
    private val coroutineDispatcher = UnconfinedTestDispatcher()

    @Before
    fun init() {
        Dispatchers.setMain(coroutineDispatcher)
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        CrumbsCore.setTestInstance(
            CrumbsCore(
                mockk(relaxed = true),
                AndroidInfoProvider(context),
                SharedPreferencesSettings.Factory(context).create("crumbs_test"),
                AndroidResourceProvider(context),
                mockk(relaxed = true),
                CrumbsOptions()
            )
        )
    }

    @After
    fun release() {
        CrumbsAndroid.get().privacy().editSettings()
            .resetDefault()
            .apply()
        CrumbsAndroid.release()
    }

    @Suppress(
        "UNUSED_ANONYMOUS_PARAMETER",
        "NAME_SHADOWING",
        "UNUSED_VARIABLE"
    ) // for documentation purposes
    @Test
    fun testInterests() {
        //tag::interests[]
        val interests = CrumbsAndroid.get().interests()
        //end::interests[]
        assertNotNull(interests)

        //tag::interestsListen[]
        val listenInterestsJob = interests.listenProfileInterests { interests ->
            Log.d("Interests", "user interests updated")
        }
        //...
        // stop listening
        listenInterestsJob.cancel()
        //end::interestsListen[]

        val tabId = "0"
        val documentUrl = "https://crumbs.org"

        //tag::interestsListenSharedInterests[]
        val listenSharedInterestsJob = interests.listenSharedInterests(
            tabId = tabId,
            documentUrl = documentUrl
        ) { sharedInterests ->
            Log.d("ShareableProfile", "${sharedInterests.size} shared interests on $documentUrl")
        }
        //...
        // stop listening
        listenSharedInterestsJob.cancel()
        //end::interestsListenSharedInterests[]

        val id = 0
        //tag::interestsResetInterest[]
        interests.resetInterest(interestId = id)
        //end::interestsResetInterest[]

        //tag::interestsSetState[]
        interests.setInterestState(interestId = id, enable = true)
        //end::interestsSetState[]

        //tag::interestsStatus[]

        val isFeatureEnable = interests.getSettings().enabled

        interests.editSettings()
            .enable(true)
            .apply()
        //end::interestsStatus[]
    }
}