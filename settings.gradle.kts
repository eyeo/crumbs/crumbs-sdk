rootProject.name = "crumbs-sdk"

pluginManagement {
    repositories {
        mavenLocal()
        gradlePluginPortal()
        google()
        mavenCentral()
        maven("https://gitlab.com/api/v4/projects/44139626/packages/maven")
    }
}
dependencyResolutionManagement {
    versionCatalogs {
        val isDevelop = System.getenv("CI_COMMIT_BRANCH") != "master"
        val versionSuffix = if (isDevelop) "-SNAPSHOT" else ""
        val isSharedFeature = System.getenv("CI_COMMIT_BRANCH")?.startsWith("feat/shared/") == true
        val versionName =
            if (isSharedFeature) "-" + System.getenv("CI_COMMIT_BRANCH").replace("feat/shared/", "")
                .replace("_", "-").lowercase() else ""

        val regex = Regex("crumbs-library-prefix += +\"(.*)\"")
        val crumbsVersion = file("gradle/libs.versions.toml").reader().useLines { lines ->
            val line = lines.first { regex.matches(it) }
            regex.find(line)!!.groupValues.last()
        }
        create("libs") {
            version("crumbs-library-full", crumbsVersion + versionName + versionSuffix)
        }
    }
    repositories {
        mavenLocal()
        google()
        mavenCentral()
        maven("https://gitlab.com/api/v4/projects/43941609/packages/maven")
        maven("https://gitlab.com/api/v4/groups/775344/-/packages/maven")
    }
}

if (System.getenv("ANDROID_HOME")?.isNotEmpty() == true ||
    System.getProperty("sdk.dir")?.isNotEmpty() == true
) {
    include(":core")
    include(":ui:android")
    include(":demo")
    include(":docs")
}