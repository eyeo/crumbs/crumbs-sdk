@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.pat.plugin)
}

pat {
    publishing {
        gitlabProjectId = "20503018"
        repositoryUrl = "https://gitlab.com/eyeo/crumbs/crumbs-sdk"
        baseArtifactId = "crumbs-ui-android"
    }
    buildConfig {
        packageName = "org.crumbs"
        configs {
            create("android")
        }
    }
}

android {
    compileSdk = libs.versions.android.target.sdk.get().toInt()
    defaultConfig {
        namespace = "org.crumbs.ui"
        minSdk = libs.versions.android.min.sdk.get().toInt()
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildFeatures {
        buildConfig = false
        viewBinding = true
    }

    testOptions {
        animationsDisabled = true
    }

    packaging {
        resources.excludes.add("META-INF/LICENSE*")
        resources.excludes.add("META-INF/*.kotlin_module")
        resources.excludes.add("META-INF/AL2.0")
        resources.excludes.add("META-INF/LGPL2.1")
        resources.excludes.add("META-INF/licenses/ASM")
        jniLibs.excludes.add("win32-x86-64/*")
        jniLibs.excludes.add("win32-x86/*")
    }

    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    api(project(":core"))
    implementation(libs.kotlin.stdlib)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.lifecycle)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.google.flexbox)
    implementation(libs.google.material)
    implementation(libs.privacy.shield.ui.android)

    androidTestImplementation(libs.pat.test)
    androidTestImplementation(libs.androidx.multidex)
}

val interestsPath = "res/raw/data_interests"

val updateInterestFile = tasks.register<ConvertInterestsTask>("updateInterestsFile") {
    interestsUrl = "https://easylist-downloads.adblockplus.org/meta-interests.json"
    outputFile = project.file("src/main/res/xml/interests.xml")
}

tasks.register<TranslateInterestsTask>("updateInterestsTranslations") {
    inputFile = project.file("src/main/res/xml/interests.xml")
    apiKey = System.getenv("GOOGLE_TRANSLATE_API_KEY")
    dependsOn(updateInterestFile)
}

val connectedAndroidTest = "connectedAndroidTest"
val deviceScreenShotsFolder = "/sdcard/Android/data/org.crumbs.ui.test/files/Download/screenshots"
val cleanScreenShotsTask = tasks.register<Exec>("cleanScreenShots") {
    commandLine("adb", "shell", "rm", "-r", deviceScreenShotsFolder)
    isIgnoreExitValue = true
}

tasks.register("updateScreenShots") {
    dependsOn("connectedAndroidTest", cleanScreenShotsTask)
    doLast {
        val outFolder = rootProject.file("docs/assets")
        File(outFolder, "screenshots").deleteRecursively()
        exec {
            commandLine("adb", "pull", deviceScreenShotsFolder)
            workingDir = outFolder
        }
    }
}

afterEvaluate {
    tasks.getByName(connectedAndroidTest).dependsOn(cleanScreenShotsTask)
}
