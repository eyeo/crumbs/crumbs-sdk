package org.crumbs.ui

import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import org.crumbs.CrumbsAndroid
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class SettingsTest {


    @Test
    fun testSettingsActivity() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext

        CrumbsAndroid.release()
        CrumbsAndroid.setup(context)
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)

        //tag::settingsActivityExtra[]
        val settingsIntent = CrumbsSettingsActivity.createIntent(
            context,
            selectedFeature = CrumbsSettingsActivity.INTERESTS_FEATURE_ID,
            parentIntent = null, // up navigation
        )
        //end::settingsActivityExtra[]

        launch<CrumbsSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is CrumbsInterestsFragment }
            Assert.assertEquals(fragment?.javaClass, CrumbsInterestsFragment::class.java)
        }

        onView(withText(R.string.crumbs_interests)).check(matches(isDisplayed()))

        CrumbsAndroid.release()
    }
}