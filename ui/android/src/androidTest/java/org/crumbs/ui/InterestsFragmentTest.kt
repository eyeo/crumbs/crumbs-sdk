package org.crumbs.ui

import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.test.FragmentScenario
import com.eyeo.privacy.PrivacyShieldAndroid
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.models.ProfileInterest
import org.crumbs.ui.utils.CrumbsFormatter
import org.crumbs.ui.utils.mockInterests
import org.hamcrest.Matchers.allOf
import org.hamcrest.core.StringContains.containsString
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class InterestsFragmentTest {

    private lateinit var data: MutableList<ProfileInterest>
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private val formatter = CrumbsFormatter.getInstance(context)


    @Before
    fun setup() {
        CrumbsAndroid.release()
        CrumbsAndroid.setup(context)
        data = mockInterests()
    }


    @After
    fun release() {
        CrumbsAndroid.release()
        CrumbsProvider.testInstance = null
    }

    @Test
    fun showInterests() {
        val scenario =
            FragmentScenario.launchInContainer<CrumbsInterestsFragment>(
                activityId = R.id.crumbs_activity_container,
                themeResId = com.eyeo.privacy.ui.R.style.Theme_PrivacyShield_Settings
            )
                .moveToState(Lifecycle.State.RESUMED)

        val enableMatcher = allOf(
            withText(containsString(formatter.getInterestDisplayName(data.first { it.enabled && it.shareable }.interest))),
            isChecked()
        )
        onView(withId(R.id.crumbs_interests_scrollview)).perform(ViewActions.swipeUp())

        onView((withId(R.id.crumbs_interests_state_rv))).perform(
            scrollTo<RecyclerView.ViewHolder>(
                enableMatcher
            )
        )

        onView(withId(R.id.crumbs_interests_scrollview)).perform(ViewActions.swipeUp())

        Thread.sleep(200)

        onView((withId(R.id.crumbs_interests_state_rv))).perform(
            scrollTo<RecyclerView.ViewHolder>(
                hasDescendant(withId(R.id.crumbs_cell_show_more_btn))
            ),
            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(
                hasDescendant(withId(R.id.crumbs_cell_show_more_btn)),
                click()
            )
        )
        val interestName =
            formatter.getInterestDisplayName(data.first { it.enabled && !it.shareable && it.hasEnoughData }.interest)
        val notShareableInterestMatcher = allOf(
            withText(containsString(interestName)),
            isChecked()
        )
        onView((withId(R.id.crumbs_interests_state_rv))).perform(
            scrollTo<RecyclerView.ViewHolder>(
                notShareableInterestMatcher
            )
        )

        val disabledInterest = data.first { !it.enabled && !it.shareable && it.hasEnoughData }
        val disabledInterestMatcher = allOf(
            withText(formatter.getInterestDisplayName(disabledInterest.interest)),
            isNotChecked()
        )
        onView((withId(R.id.crumbs_interests_state_rv))).perform(
            scrollTo<RecyclerView.ViewHolder>(
                disabledInterestMatcher
            )
        )

        scenario.moveToState(Lifecycle.State.DESTROYED)
    }

}