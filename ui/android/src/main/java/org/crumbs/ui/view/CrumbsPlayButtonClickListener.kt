package org.crumbs.ui.view

import com.eyeo.privacy.ui.view.PrivacyShieldPlayButton
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext
import org.crumbs.CrumbsProvider

class CrumbsPlayButtonClickListener(private val uiContext: PrivacyShieldUIContext) :
    PrivacyShieldPlayButton.ClickListener, CrumbsProvider {
    override fun onClick() {
        val url = uiContext.getCurrentUrl()
        interests?.apply {
            val editor = editSettings()
            when (uiContext.getProtectionStatus()) {
                PrivacyShieldUIContext.ProtectionStatus.ENABLED -> {
                    editor.disableDomain(url)
                }

                PrivacyShieldUIContext.ProtectionStatus.PAUSED,
                PrivacyShieldUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> {
                    editor.enableDomain(url)
                }
            }.apply()
        }
    }

    override fun onLongClick() {
        val url = uiContext.getCurrentUrl()
        interests?.apply {
            editSettings().disableDomainThisSession(url).apply()
        }
    }
}