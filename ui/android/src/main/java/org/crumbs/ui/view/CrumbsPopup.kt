package org.crumbs.ui.view

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.eyeo.pat.utils.ContextExtension.getActivity
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.ui.analytics.UiEvent.Popup
import com.eyeo.privacy.ui.view.PrivacyShieldPlayButton
import com.eyeo.privacy.ui.view.PrivacyShieldPopupView
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsSettingsActivity
import org.crumbs.ui.R
import org.crumbs.ui.analytics.UIEvent.InterestsPopup
import com.eyeo.privacy.ui.R as PsR

class CrumbsPopup(context: Context) : PopupWindow(), PrivacyShieldPopupView.NavigationListener,
    CrumbsProvider {

    private val popupView: PrivacyShieldPopupView
    private val crumbsInterestsBoardView: CrumbsInterestsBoardView

    init {
        isFocusable = true
        isOutsideTouchable = true
        setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        width = context.resources.getDimensionPixelSize(com.eyeo.privacy.ui.R.dimen.ps_popup_width)
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        popupView = PrivacyShieldPopupView(
            context,
            logoBackgroundResId = R.drawable.crumbs_logo_text,
            logoForegroundResId = R.drawable.crumbs_logo_dot,
            titleResId = null
        )
        contentView = popupView

        popupView.setClickListener(object : PrivacyShieldPopupView.ClickListener {
            override fun onSettingsButtonClick() {
                context.startActivity(
                    CrumbsSettingsActivity.createIntent(
                        context,
                        parentIntent = context.getActivity()?.intent
                    )
                )
            }
        })
        val popupContainer =
            contentView.findViewById<LinearLayout>(PsR.id.ps_board_view_parent)
        crumbsInterestsBoardView = CrumbsInterestsBoardView(contentView.context)
        crumbsInterestsBoardView.setMoreListener {
            context.startActivity(
                CrumbsSettingsActivity.createIntent(
                    context,
                    CrumbsSettingsActivity.INTERESTS_FEATURE_ID,
                    context.getActivity()?.intent
                )
            )
            dismiss()
            requireCrumbs.analytics().notifyEvent(Event.InterestsPopup.PRESS_MORE_INTERESTS)
        }
        popupContainer.addView(crumbsInterestsBoardView)

        animationStyle = com.eyeo.privacy.ui.R.style.PrivacyShieldPopupAnimation
    }

    @JvmOverloads
    fun show(anchor: View, uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default) {
        val padding = (12 * Resources.getSystem().displayMetrics.density).toInt()
        popupView.setPrivacyShieldUIContext(uiContext)
        popupView.setNavigationListener(this)

        val playButton =
            popupView.findViewById<PrivacyShieldPlayButton>(PsR.id.ps_board_play_btn)
        playButton.addClickListener(CrumbsPlayButtonClickListener(uiContext))

        showAsDropDown(anchor, -padding, -padding)
        update()
        crumbs?.analytics()?.notifyEvent(Event.Popup.OPEN_SCREEN)
    }

    override fun onLinkOpen() {
        dismiss()
    }

}