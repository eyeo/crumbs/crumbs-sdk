package org.crumbs.ui.analytics

import com.eyeo.privacy.analytics.Event

/**
 * CrumbsInterestsFragment events
 */
object UIEvent {
    //tag::eventIds[]
    object InterestsSettingsKeys {

        /**
         * Interests settings fragment has been opened
         */
        const val OPEN_SCREEN = "interests_settings_open_screen"

        /**
         * Search bar focused
         */
        const val FOCUS_SEARCH = "interests_settings_focus_search"

        /**
         * Toggle interests state in the profile list
         * Event value type is ProfileInterest
         */
        const val TOGGLE_INTEREST_STATE = "interests_settings_toggle_interest_state"


        /**
         * Swipe interest to forget it
         * Event value type is ProfileInterest
         */
        const val SWIPE_INTEREST = "interests_settings_swipe_interest"

        /**
         * Validate forget interest warning popup
         * Event value type is ProfileInterest
         */
        const val FORGET_INTEREST = "interests_settings_forget_interest"
    }

    /**
     * CrumbsPopup events
     */
    object InterestsPopupKeys {

        /**
         * More interests button has been pressed
         */
        const val PRESS_MORE_INTERESTS = "popup_press_more_interests"
    }
    //end::eventIds[]

    val Event.Companion.InterestsSettings
        get() = InterestsSettingsKeys
    val Event.Companion.InterestsPopup
        get() = InterestsPopupKeys
}