package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext
import org.crumbs.CrumbsProvider
import org.crumbs.models.CrumbsInterest
import org.crumbs.ui.R
import org.crumbs.ui.databinding.CrumbsViewInterestsBinding

class CrumbsInterestsBoardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), PrivacyShieldUIContext.UIContextListener, CrumbsProvider {

    private val binding: CrumbsViewInterestsBinding
    private var interestsJob: Cancellable? = null

    init {
        binding = CrumbsViewInterestsBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun setInterests(domain: String, categories: List<CrumbsInterest>) {
        val hasData = categories.isNotEmpty()
        binding.crumbsInterestsEmptyGroup.visibility = if (hasData) View.GONE else View.VISIBLE
        binding.crumbsInterestsEnableGroup.visibility = if (hasData) View.VISIBLE else View.GONE
        binding.crumbsInterestsSubtitle.text =
            context.getString(R.string.crumbs_interests_shared_with, domain.getUrlHost() ?: domain)
        binding.crumbsInterestsList.setInterests(categories, null)
        binding.crumbsInterestsEmptySubtitle.text =
            context.getString(R.string.crumbs_not_shared, domain.getUrlHost() ?: domain)
    }

    override fun onTabChanged(tabId: String, documentUrl: String) {
        this.interestsJob?.cancel()
        this.interestsJob = interests?.listenSharedInterests(tabId, documentUrl) {
            setInterests(documentUrl, it.toList())
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        interestsJob?.cancel()
    }

    fun setMoreListener(listener: OnClickListener) =
        binding.crumbsInterestsMoreBtn.setOnClickListener(listener)
}