package org.crumbs.ui

import com.eyeo.privacy.ui.PrivacyShieldUI
import org.crumbs.CrumbsCore

object CrumbsCoreExtension {
    fun CrumbsCore.ui() = PrivacyShieldUI.get()
}