plugins {
    id("org.jetbrains.kotlin.jvm") version "1.7.20"
}
repositories {
    mavenCentral()
}

dependencies {
    implementation("com.squareup:kotlinpoet:1.10.2")
    implementation("io.ktor:ktor-client-core:2.2.2")
    implementation("io.ktor:ktor-client-cio:2.2.2")
    implementation("io.ktor:ktor-client-content-negotiation:2.2.2")
    implementation("io.ktor:ktor-serialization-gson:2.2.2")
    implementation("io.ktor:ktor-client-logging:2.2.2")
}